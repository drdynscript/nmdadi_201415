/*
Created by: Philippe De Pauw - Waterschoot
Date: 21-10-2014
Name: app.js
Description: Tweets application
*/

//Enumertation of moods
var Moods = {
    HAPPY:1,
    SAD:2,
    properties:{
        1:{id:1, name:'Happy'},
        2:{id:2, name:'Sad'}
    }
};

//Create the AppData object for the Twee Application
var AppData = {
    "information":{
        "title":"Tweet Application",
        "version":"1.0",
        "modified":"21-10-2014",
        "author":"Philippe De Pauw - Waterschoot"
    },
    "tweets":[],
    "settings":{}
};

(function(){
    //Cache important elements from the DOM
    var txtTweetTextArea = document.querySelector('#txtTweet');
    var txtTweetCharAmount = document.querySelector('.txtTweetCharAmount');
    var tweetsAll = document.querySelector('#tweets-all');
    var tweetsAllPositive = document.querySelector('#tweets-all-positive');
    var tweetsAllNegative = document.querySelector('#tweets-all-negative');
    var newTweetWrapper = document.querySelector('#newtweet-wrapper');
    var newTweetForm = document.querySelector('#formNewTweet');

    //Listen to important events in/on TextArea
    if(txtTweetTextArea != null && txtTweetCharAmount != null){
        //TextArea gets the focus -> listen to it!
        txtTweetTextArea.addEventListener('focus', function(ev){
            //Open or close the wrapper --> Form
            newTweetWrapper.classList.toggle('open');
        });
        //Writing within a TextArea --> Listen to it!
        txtTweetTextArea.addEventListener('input', function(ev){
            var content = Utils.trim(this.value);
            txtTweetCharAmount.textContent = (256-content.length);
        });
    }

    //Submit the Tweet form --> listen to it!
    newTweetForm.addEventListener('submit', function(ev){
        //Dismiss the default browser eventlistener for submit event
        ev.preventDefault();

        //Get selected value of RadioButtonGroup
        var rdbtnGroupMood = document.querySelector('.rdbg-mood input[type="radio"]:checked');

        //Get the content from the TextArea and Trim it!
        var content = Utils.trim(txtTweetTextArea.value);

        //Create a new Tweet Object
        var tweet = new Object();
        tweet.id = Utils.guid();
        tweet.content = content;
        tweet.mood = (rdbtnGroupMood != null)?convertMoodStringToMood(rdbtnGroupMood.value):Moods.HAPPY;
        tweet.created = new Date().getTime();

        if(AppData != null){
            if(AppData.tweets == null){
                AppData.tweets = [];
            }
            //Add tweet to the tweets Array within AppData
            AppData.tweets.push(tweet);
            //Update the User Interface --> to call
            updateTweetsUI();
            //Update corresponding data in the localstorage
            Utils.store('dds.tweetapp', AppData);
        }

        return false;
    });

    //Function: Update the User Interface
    function updateTweetsUI(){
        if(AppData != null && AppData.tweets != null){
            //Sorting the Array and reverse
            AppData.tweets = _.sortBy(AppData.tweets, 'created').reverse();

            //Write to tweetsAll
            createTweetsInContainer(tweetsAll, null);
            //Write to tweetsAllPositive
            createTweetsInContainer(tweetsAllPositive, Moods.HAPPY);
            //Write to tweetsAllNegative
            createTweetsInContainer(tweetsAllNegative, Moods.SAD);

            //Register listeners for all the tweets
            var tweetNodes = document.querySelectorAll('.tweet');
            if(tweetNodes != null && tweetNodes.length > 0){
                _.each(tweetNodes,function(tweetNode){
                    tweetNode.querySelector('.tweet-remove')
                        .addEventListener('click', function(ev){
                            //Get the id of the tweet by the dataset property of the data attributes
                            var tweetId = this.parentNode.dataset.id;
                            //Call the function: removeTweetById
                            removeTweetById(tweetId);
                        });
                });
            }
        }
    }

    //Function: Convert Mood to Fontawesome Icon
    function convertMoodToFAIcon(mood){
        switch(mood){
            case Moods.HAPPY:return '<i class="fa fa-smile-o"></i>';
            case Moods.SAD:return '<i class="fa fa-meh-o"></i>';
            default:return '<i class="fa fa-question"></i>';
        }
    }

    //Function: Convert Mood String to Mood
    function convertMoodStringToMood(moodStr){
        switch(moodStr){
            case 'Happy':return Moods.HAPPY;
            case 'Sad':return Moods.SAD;
            default:return Moods.HAPPY;
        }
    }

    //Function: Remove a specific Tweet By his id
    function removeTweetById(tweetId){
        if(AppData != null && AppData.tweets != null && AppData.tweets.length > 0){
            //Find the tweet in the Array by his id
            var index = _.findIndex(AppData.tweets,function(tweet){
               return tweet.id == tweetId;
            });

            if(index > -1){
                //Remove the tweet from the Array
                AppData.tweets.splice(index, 1);
                //Update the User Interface --> to call
                updateTweetsUI();
                //Update corresponding data in the localstorage
                Utils.store('dds.tweetapp', AppData);
            }
        }
    }

    //Function: Get HTML for a Tweet
    function getHTMLForTweetById(tweetId){
        var tweet = _.find(AppData.tweets,function(tweet){
           return tweet.id == tweetId;
        });

        var htmlContent = '';
        if(tweet != null){
            htmlContent += ''
                + '<article class="tweet" data-id="' + tweet.id + '">'
                + '<p class="tweet-content">' + tweet.content + '</p>'
                + '<span class="tweet-mood">' + convertMoodToFAIcon(tweet.mood) + '</span>'
                + '<span class="tweet-date">' + Utils.timeToTwitterDateTimeString(tweet.created) + '</span>'
                + '<span class="tweet-remove"><i class="fa fa-remove"></i></span>'
                + '</article>';
        }
        return htmlContent;
    }

    //Function: Create Filtered Tweets on a specified container with a specified filter
    function createTweetsInContainer(container, mood){
        if(container != null){

            var filteredTweets = AppData.tweets;

            if(mood != null){
                filteredTweets = _.filter(AppData.tweets, function(tweet) { return tweet.mood == mood });
            }

            //Clear the contents of the container
            container.innerHTML = '';
            //Temp variable for content
            var htmlContent = '';
            //Loop through the Array of tweets
            _.each(filteredTweets, function(tweet){
                //Get HTML for certain tweet and add it to the Temp variable htmlContent
                htmlContent += getHTMLForTweetById(tweet.id);
            });
            //Add Temp variable to the DOM
            container.innerHTML = htmlContent;
        }
    }

    //Get the AppData from the datastorage: localstorage
    if(Utils.store('dds.tweetapp') != null){
        AppData = Utils.store('dds.tweetapp');
    } else{
        Utils.store('dds.tweetapp', AppData);
    }

    //Update the User Interface --> to call
    updateTweetsUI();

})();