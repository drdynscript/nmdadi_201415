/*
Created by: Philippe De Pauw - Waterschoot
Date: 14-10-2014
Name: app.js
Description: JavaScript for the application
*/
(function(){
    var wildIctetjen;//Declaratie
    console.log(wildIctetjen);

    var age = 113;//46.235
    var g_1 = NaN;
    var isFemale = false;
    var dataset = null;
    console.log(dataset);

    //Object, Array, Math, Date, RegExp
    //console.log(a);

    var c;
    if(c === undefined){
        console.log('c is undefined!');
    }else{
        console.log('c is defined!');
    }

    if(!c){
        console.log('c is undefined!');
    }

    try{
        console.log(d);
    }
    catch(ex)
    {

    }
    console.log(1 === true);

    var v_1 = null;
    console.log(v_1/6);//0

    var x, y = 5, z;

    var olod = 'NMDADI';
    var olod;
    console.log(olod);

    //SCOPE
    //=====================
    var nFruit = 16, nVegetable = 7;
    if(nFruit > nVegetable){
        var nFruityPeople = 18;
    }else{
        var nVegetablyPeople = 3;
    }
    console.log(nVegetablyPeople);

    function TestCar(){
        var carSpeed = 469, carGear = 4;
        carColors = 'uglygreen';
        if(carSpeed > 200){
            var carName = 'Herbie';
        }else{
            var carSound = 'Kapot';
        }
        console.log(carSound);
    }
    TestCar();
    console.log(carColors);

    //Math operators
    //+, -, /, *, %, ++, --

    //Assignment operators
    //=, +=, -=, /=, *=, %=

    //Comparison operators
    //==, === , !=, !==, >=, <=, >, <

    //Logical operators
    //&&, ||, !

    //Conditional operator
    var isGamePlaying = false, msg;
    if(!isGamePlaying){
        msg = 'Game Over. Start a new game please...';
    }else{
        msg = 'I\'m playing the game...';
    }
    console.log(msg);

    msg = (!isGamePlaying)?'Game Over. Start a new game please...':'I\'m playing the game...';

    console.log(msg);

    //Statements
    //if, if/else, if else, if/else, switch/case
    //for, for/in, while, do/while, foreach
    //break, continue, try/catch, throw
    var direction = 'SLA', dirMsg;
    switch(direction){
        case 'N':
            dirMsg = 'North';
            break;
        case 'NO':
            dirMsg = 'North-East';
            break;
        default:
            dirMsg = 'South';
            break;
    }
    console.log(dirMsg);

    var academicYears = [];
    academicYears[0] = '2010-11';
    academicYears[1] = '2011-12';
    academicYears[2] = '2012-13';

    console.log(typeof academicYears);
    console.log(academicYears.length)

    var fruits = ['Apple','Orange','Cat','Banana'];
    console.log(fruits.toString());
    console.log(fruits);
    console.log(fruits[2]);
    fruits.pop();
    console.log(fruits);
    fruits.push('Banana','Melon');
    console.log(fruits);
    var apple = fruits.shift();
    console.log(apple + ' - ' + fruits);
    fruits.unshift('Apple','Berry');
    console.log(fruits);
    var index = fruits.indexOf('Cat');
    var cat = fruits.splice(index, 1);
    console.log(cat + ' - ' + fruits);
    var yummiFruit = fruits.slice(2,4);
    console.log(yummiFruit);

    //Literal Notation
    var person = {
        'firstName':'Philippe',
        666:'The Devil',
        toString:function(){
            return this.firstName + ': ' + this['666'];
        }
    };
    console.log(person['666']);
    console.log(person['toString']());

    var femGent = [];
    femGent['2012'] = 98000;
    femGent['2014'] = 145000;

    console.log(femGent['2014']);

    var matrix = [
        [1,6,9],
        [2,4,8],
        [3,5,7]
    ];

    matrix[1][1];//4

    console.log(matrix.join('\n'));

    //JSON Notation
    var persons = {
        "students":[
            {
                "firstName":"Olivier",
                "surName":"Parent",
                "age":33,
                "addresses":[
                    {
                        "street":"Industrieweg 232",
                        "isHome":false
                    },
                    {
                        "street":"KrapuulStroate 16",
                        "isHome":true
                    }
                ]
            },
            {
                "firstName":"Jolien",
                "surName":"Pottie",
                "age":18,
                "addresses":[
                    {
                        "street":"Hoogpoort 15",
                        "isHome":true
                    },
                    {
                        "street":"KrapuulStroate 16",
                        "isHome":true
                    }
                ]
            }
        ]
    };

    console.log("Adres van " + persons.students[1].firstName + ': ' +persons.students[1].addresses[0].street);

    var jsonString = JSON.stringify(persons);
    console.log(jsonString);
    console.log(JSON.parse(jsonString));
})();