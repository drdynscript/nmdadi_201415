/*
Created by: Philippe De Pauw - Waterschoot
Date: 04-11-2014
Name: app.js
Description: OOP in JavaScript
*/

/*
Object Initializers
 */
var movie = {
    "title":"Lock, Stock and Two Smoking Barrels",
    "synopsis":"A botched card game in London triggers four friends, thugs, weed-growers, hard gangsters, loan sharks and debt collectors to collide with each other in a series of unexpected events, all for the sake of weed, cash and two antique shotguns.",
    "year":1998,
    "toString":function(){
        return this.title;
    }
};
console.log(movie);//Get the object
console.log(movie.toString());//Get the string representation of the object

//Make a clone of the previous object (movie)
var movie2 = movie;
movie2.title = "De reis van Chihiro";
movie2.synopsis = "In the middle of her family's move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and monsters; where humans are changed into animals; and a bathhouse for these creatures.";
movie2.year = 2001;
console.log(movie2);

//Make a Shittyplace Object with Literal notation
var shittyplace = {
    "type":"Urinoir",
    "geolocation":{"lat":51.053368,"lng":3.73037},
    "toString":function(){
        return this.type + ': (' + this.geolocation.lat + ', ' + this.geolocation.lng + ')';
    }
};
console.log(shittyplace);//Output the object in the Console
console.log(shittyplace.toString());

/*
Object Constructors
Description of a Person
= class in OOP
 */
function Person(){
    this.firstName;
    this.surName;
    this.isFemale = true;
    this.toString = function(){
        return this.firstName + ' ' + this.surName;
    }
}
//Create a Person -> Object
var p = new Person();//Initialize
p.firstName = "Philippe";
p.surName = "De Pauw - Waterschoot";
p.isFemale = false;
console.log(p);
console.log(p.toString());

//Simulate Encapsulation in Object Constructor
function ApplicationUser(){
    //Private variables
    var _userName;
    var _password;
    var _email;

    //Private function or method
    function toString(){
        return _userName + ' -> Email: ' + _email;
    }

    //Public methods
    return {
        toString:toString,
        getUserName:function(){
            return _userName;
        },
        setUserName:function(value){
            _userName = value;
        },
        getEmail:function(){
            return _email;
        },
        setEmail:function(value){
            _email = value;
        }
    };
}
//Make an instance of the ApplicationUser (class)
var user1 = new ApplicationUser();
user1.setUserName('Philippe');
user1.setEmail('De Pauw - Waterschoot');
console.log(user1.toString());