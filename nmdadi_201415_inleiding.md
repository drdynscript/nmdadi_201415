﻿NMDAD-I 2014-15
===============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|
|Hogeschool|Arteveldehogeschool|

***

[TOC]

***

|Info||
|--|--|
|Studiepunten|6|
|Contacturen|4|
|Docent|Philippe De Pauw - Waterschoot|
|Evaluatie 1ste examenkans|Werkstuk(ken) (maximaal in groepjes van 3). Mondelinge verdediging van werkstuk(ken) + vragen|
|Evaluatie 2de examenkans|Werkstuk(ken) (Individueel). Mondelinge verdediging van werkstuk(ken) + vragen|
|Begincompetenties|2D Animation & Webdesign II|
|ECTS|[ECTS Fiche NMDADI](http://www.arteveldehogeschool.be/ects/ahsownapp/ects/ECTSFiche.aspx?olodID=42086)|

##Studenten & groepsindeling

**2MMPa alias proDEV (35 studenten)**

- Beelprez Seppe
- Bultynck Laurens
- Chanterie Neeltje
- Decock Aurelio
- De Jaeghere Maarten
- Demey Wendy
- De Rammelaere Stijn
- Desmedt Guillaume
- Fauconnier Frederik
- Goossens Bert
- Knockaert Jeroen
- Kumeling Thomas
- Lambelin Rafaël
- Mares Quinten
- Mergan Lien
- Onderbeke Mattis
- Schepens Robbe
- tiberghien Jeffrey
- Vanbrabant Wesley
- Van den Broecke Thibault
- Vandewalle Michiel
- Vandoorne Lennart
- Van Droogenbroeck Lucas
- van Grinsven Thomas
- Vanhecke Nicolas
- Vanhove Maxim
- Van Meirvenne Tim
- Van Wienendaele Stef
- Verbeke Alan
- Vercruysse Aline
- Verplanken Valérie
- Verschaeren Dieter
- Vervaet Ellen
- Vindevogel Yordi
- Wathelet Vincent

**2MMPb alias proDUCE (30 studenten)**

- Acar Zehra
- Aernout Niklaas
- Albrecht Brian
- Aliaj Gazmend
- Asemota Osayuki
- Broekaert Thomas
- Bruggeman Eliaz
- Claes Jens
- Claeys Thomas
- Coen Hannelore
- David Ruben
- De Boeck Inge
- De Kerpel Glenn
- Dekeyser Pieterjan
- De Muyt Ine
- De Neve Laurens
- Gulickx Michiel
- Haentjens Nils
- Lissens Jolien
- Moes Wim
- Mortier Bavo
- Roelandt Celine
- Stalpaert Lien
- Thibau Jeroen
- Van Belle Michiel
- Van der Meeren Vincent
- Vannieuwenhuyse Zymcke
- Van Overberghe Ben
- Vinjé Jonathan
- Yang Michael
- Zwart Mathias
- Brecht Nolf

**2MMP onbekend**

- Zemarai Mohammad
- Dils Tom
- Calberson Roë
- Denteneer Rik
- Kai Hostens
- De Cuypere Lorenz

**Groepsindeling 2MMPb Opdracht 1: Cocoa**

|Groep|Studenten|
|-----|---------|
|Groep1|Aliaj Gazmend, De Kerpel Glenn, Van der Meeren Vincent|
|Groep2|Mortier Bavo, Gulickx Michiel, Broekaert Thomas|
|Groep3|Bruggeman Eliaz, Zwart Mathias, Van Belle Michiel|
|Groep4|Stalpaert Lien, Claeys Thomas|
|Groep5|Aernout Niklaas, David Ruben|
|Groep6|Yang Michael, Vinjé Jonathan, Vannieuwenhuyse Zymcke|
|Groep7|Albrecht Brian, De Boeck Inge, Van Overberghe Ben|
|Groep8|Coen Hannelore, Roelandt Celine|
|Groep9|Thibau Jeroen, De Muyt Ine, Dekeyser Pieterjan|
|Groep10|Lissens Jolien, Brecht Nolf, Van den Broecke Thibault|
|Groep11|De Neve Laurens, Haentjens Nils|
|Groep12|Beelprez Seppe|
|Groep13|De Rammelaere Stijn, Claes Jens|
|Groep14|Acar Zehra|
|Groep15|Asemota Osayuki|

**Groepsindeling 2MMPa Opdracht 1: Cocoa**

|Groep|Studenten|
|-----|---------|
|Groep1|De Jaeghere Maarten, Raeymaekers Quinten|
|Groep2|Mares Quinten, Mergan Lien, van Grinsven Thomas|
|Groep3|Vandewalle Michiel, Van Wienendaele Stef, Goossens Bert|
|Groep4|Dils Tom, Onderbeke Mattis|
|Groep5|Schepens Robbe, Verbeke Alan, Vervaet Ellen|
|Groep6|Vanhecke Nicolas|
|Groep7|Vercruysse Aline, Wathelet Vincent, tiberghien Jeffrey|
|Groep8|Knockaert Jeroen, Desmedt Guillaume, Vandoorne Lennart|
|Groep9|Denteneer Rik, Kai Hostens|
|Groep10|Chanterie Neeltje, Demey Wendy, Verplanken Valérie|
|Groep11|Vanbrabant Wesley, Vanhove Maxim, De Cuypere Lorenz|
|Groep12|Verschaeren Dieter, Kumeling Thomas, Van Droogenbroeck Lucas|
|Groep13|Vindevogel Yordi, Bultynck Laurens, Van Meirvenne Tim|
|Groep14|Lambelin Rafaël|

##Inhoud

- **Inleiding**
- **Markdown**
- Dataformaten: CSV, XML, XSL-T, XPath, XSL-FO, JSON(P), BSON, SVG, SMIL, MathML
- **Mappenstructuur van een front-end webapplicatie**
- HTML5: Global attributes, Semantische elementen, Geolocation, Canvas
- Responsive webdesign, mobile first/desktop first (zonder framework)
- Responsive navigations, images, video's en andere componenten
- Icons: Favicon, Touch icons, Sprites
- Font iconen
- Uitbreiding CSS3
- Prototype-based programming (OOP)
- AMD (Asynchronous Module Definition)
- Consuming Data met JavaScript
- User Experience (UX) Design

##Studiemateriaal

- Chamilo cursus NMDAD-I
- Bitbucket Respository [NMDADI_201415](https://bitbucket.org/drdynscript/nmdadi_201415)
- Online links, tutorials and samples

##Tools

- <http://git-scm.com/>
- <http://www.sourcetreeapp.com/>
- <http://www.jetbrains.com/phpstorm/>
- <https://bitbucket.org/>
- <http://courses.olivierparent.be/servermanagement/local-development-environment/installing-design-and-development-tools/source-control/>

##Technologieën

- HTML
- CSS
- JavaScript
- CSS pre-processors
- Static Site Generators
- Commandline and Automation tools
- JS bibliotheken & frameworks, zoals: jQuery, Normalize, Modernizr, Respond.js, Underscore.js, Lodash.js, Crossroads.js, Hashes.js, ...

##Software

- **Adobe CC**
- SublimeText
- **PHP Storm 7+**
- ...

##Code PHPStorm

Zie chamilo bij de aankondigingen cursus NMDAD-I


##Opdracht 1: Remember the cocoa!


|Info||
|----|--|
|Werktitel|Remember the cocoa!|
|Subtitel|Beheer van taken en takenlijsten|
|Milestone 1|**12-11-2014** Presentatie tussentijds productiedossier|
|Deadline 1|**29-11-2014** Opleveren opdracht 1|
|Deadline 2|**26-12-2014** Opleveren opdracht 1: Versie 2|
|Examen|Afgedrukt dossier, Poster, DVD|

###Tussentijds productiedossier (milestone 1)

- Moet geschreven worden in Markdown (Stackedit.io + Google Sync)
- Briefing
- Analyse
- Technische specificaties
- Functionele specificaties
- Persona's (+ scenario)
- Moodboard
- Sitemap
- Wireframes

> Presentatie 10min/groep

###Technische specificaties

- Versiebeheer
	- Maak een nieuw Git Repository voor je project aan op **Bitbucket**.
Code worden tussen groepen niet gedeeld!
Deel het project met de docent(en).
- Frontend
	- HTML5, CSS3 en JavaScript
	- jQuery, underscore.js, lodash.js, crossroads.js, js-signals
	- localstorage of IndexedDB

> Webapplicatie moet de look-and-feel van een native applicatie! Het responsive- framework alsook alle andere bestanden moeten zelf geïmplementeerd worden. Tijdsbesteding moet in het dossier aanwezig zijn!

- <http://www.pttrns.com/>
- <http://www.mobile-patterns.com/>

###Persona's

- Gebruiker (User)

###Verplichte features

- User kan lijsten beheren
- User kan categorieën beheren (Vb.: Personal, Work, Hobby, ...)
- User kan een categorie (0..1) aan een lijst toekennen (Vb.: Lijst carrefour 22-10-2014, bevat de categorie Personal)
- User kan taken toekennen aan een lijst
- User kan een categorie (0..1) aan een taak (die **niet** in een lijst zit) toekennen
- User kan taken beheren
- User kan taken voltooien en deze voltooing ongedaan maken
- User kan prioriteiten instellen voor de taken binnen een lijst en voorziet kleurcodes per prioriteit
- User kan due-date beheren voor iedere taak
- User beheert reminders
- Extra features zonder bugs levert extra punten!
- Automation verplicht!
    - Componenten worden via Bower toegevoegd in de components folder van de app folder
    - SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
    - CSS-bestanden worden met elkaar verbonden in één bestand en geminified
    - De JS code wordt automatisch nagekeken op syntax fouten
    - JS-bestanden worden met elkaar verbonden in één bestand en geminified
    - De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
    - Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken


###Opleveren

- DVD (ISO bestand - Naam ISO bestand: **nmdadi_201415_cocoa_2MMP{a of b}_groep{1...n}_v2.iso**)
    - README.md (Markdown-bestand met informatie van de groepsleden en groepsnummer)
    - brondbestanden (InDesign, Illustrator, Photoshop, ...)
    - documents (folder)
        - dossier.pdf
        - dossier.md
        - poster.pdf
        - poster.png
    - screenshots (folder)
    - screencasts (folder)
    - project (folder - zie structuur frontend webapplicatie - zie structuur bitbucket)
        - app
        - dist
        - test
        - ...
- Print (afgedrukt)
    - Productiedossier (Opmaak in in-design)
    - A2 Academische Poster
- Bitbucket (Naam repository:  **nmdadi_201415_cocoa_2MMP{a of b}_groep{1...n}_v2**)
    - project_cocoa_code
        - app
            - bower_components (in de app of op een niveau hoger)
            - documents
            - fonts (webfonts)
            - images
                - icons
                    - favicon.ico
                    - verschillende touch iconen
            - scripts
                - app.js
                - googlemaps.js
                - helpers.js
                - models.js
                - services.js
                - utilities.js
            - styles
                - app.css
                - buttons.css
                - colors.css
                - common.css
                - helpers.css
                - layout.css
                - lists.css
                - normalize.css
                - ...
            - 404.html
            - humans.txt
            - index.html
            - manifest.webapp
            - robots.txt
            - sitemap.xml
        - dist
        - docs
            - install.md
        - test
        - screenshots
        - .editorconfig
        - .bowerrc
        - .gitattributes
        - .gitignore
        - .jscsrc
        - .jshintrc
        - bower.json
        - gulpfile.js
        - LICENSE
        - package.json
        - README.md
    - project_cocoa_docs
        - dossier.md
        - dossier.pdf
        - poster.pdf
        - screencast.mpeg
        - screenshot_320.png
        - screenshot_480.png
        - screenshot_640.png
        - screenshot_800.png
        - screenshot_960.png
        - screenshot_1024.png
        - screenshot_1280.png

##Opdracht 2: Open the gates for data.

ANNULATIE!!! (Opdracht 1 moet finaal en subliem afgewerkt zijn)