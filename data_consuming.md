Laden en consumeren van data
============================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

XMLHttpRequest
--------------

Het `XMLHttpRequest` object is een gestandardiseerd JS object om op een eenvoudige manier HTTP of HTTPS aanvragen te versturen naar een server en vervolgens het antwoord (response) van deze server terug te laden in JS. Data kan verstuurd worden naar een specifieke URL, wordt verwerkt door de server en deze stuurt vervolgens andere data door naar de client zonder dat de webpagina wordt herladen. Deze data wordt als het ware uitgewisseld tussen de client en een server. Gedeelten van een webpagina kunnen dus geüpdatet worden zonder de webpagina volledig te herladen. De client ontvangt de data niet direct van de server, omdat de server meestal meerdere aanvragen moet verwerken. Vandaar dat deze aanvragen meestal asynchroon worden verzonden en ontvangen. Dit is beter gekend onder de benaming AJAX, wat betekent: **"Asynchrone JavaScript XML"**. Ondanks de naam, kan het `XMLHttpRequest` object ook gebruikt worden om elke soort data, dus niet alleen XML, op te halen van- en de versturen naar de server.

De server fungeert als een **Web API** die aanvragen verwerkt, instructies uitvoert en vervolgens antwoorden verstuurt naar de clients. De verstuurde antwoorden kunnen van verschillend datatypen zijn: Plain text, XML, (X)HTML, JSON(P), BLOB, JPG, GIF, PNG, ... . Zo'n Web API kan omschreven worden als een REST- of RESTful service.

Om een instantie te maken van het `XMLHttpRequest` object:

```javascript
var xhr = new XMLHttpRequest();
```

IE5 en IE6 ondersteunen dit object niet, dit moeten we opvangen via een specifiek  `ActiveXObject` object.

```javascript
var xhr = typeof XMLHttpRequest != undefined
    ? new XMLHttpRequest()
    : new ActiveXObject('Microsoft.XMLHTTP');
```

Na het instantiëren van het `XMLHttpRequest` object, openen we een connectie naar een bepaalde URL via de `open` methode. Deze methode bevat minimaal twee parameters:

```javascript
open(HTTP methode, URL, asynschroon[, Gebruikersnaam, Paswoord])
```

De eerste parameter is een string waarbij we de te gebruiken HTTP methode definiëren:

- `GET`  
Ophalen van data. Indien we data meesturen met de aanvragen, dan zal deze data zichtbaar zijn in de URL.
- `POST`  
Vesturen en ophalen van data. Deze data is niet zichtbaar in de URL. Wordt meestal gebruikt om nieuwe elementen in de database van de server aan te maken.
- `PUT`  
Vesturen en ophalen van data. Deze data is niet zichtbaar in de URL. Wordt meestal gebruikt om bestaande elementen in de database van de server aan te wijzigen.
- `DELETE`  
Vesturen en ophalen van data. Deze data is niet zichtbaar in de URL. Wordt meestal gebruikt om bestaande elementen in de database van de server aan te verwijderen.
- `HEAD`
- `OPTIONS`

De tweede parameter is een string waarin we de `URL` van het HTTP request definiëren.

|URL|Omschrijving|
|---|------------|
|http://datatank.gent.be/MilieuEnNatuur/Ecoplan.json|Ecoplan Stad Gent|
|http://datatank.gent.be/Cultuur-Sport-VrijeTijd/GentseFeestenLocaties.json|Gentse Feesten Locaties|
|http://datatank.gent.be/Grondgebied/Wijken.json|Wijken in Stad Gent|
|http://datatank.gent.be/MilieuEnNatuur/Parken.json|Parken Stad Gent
|https://query.yahooapis.com/v1/public/yql?q=select%20\*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys|Yahoo Weather API|
|http://api.worldbank.org/topic/1?per_page=100&format=json|Worldbank Agriculture & Rural Development|

De derde parameter is een boolean waarde waarbij we aangeven of de aanvraag sunchroon of asynchroon verloopt. Deze parameter is optioneel en heeft als standaard waarde `true`, waardoor de aanvraag asynchroon verloopt. Een synchrone aanvraag, `false`, invokeert de  `onreadystatechange` gebeurtenis luisteraar niet!

De laatste twee parameters worden gebruikt om de cliënt te authentificeren en autoriseren.  

Om de aanvraag of request te versturen moeten we de `send` methode aanspreken van het `XMLHttpRequest` object. Deze methode bevat één parameter, namelijk de data die naar de server verstuurd moet worden.

```javascript
send(data)
```

Wensen we geen data te sturen naar de server, dan geven we `null` als waarde aan deze parameter.

Wanneer we de aanvraag asynchroon versturen kunnen gebruik maken van de `onreadystatechange` gebeurtenis luisteraar (event listener). Deze luisteraar wordt telkens geïnvokeerd indien een bepaalde status plaats zal vinden tijdens het afhandelen van de aanvraag door de server.

Het veranderen van deze statussen gaat als volgt:

- Nadat de `open` methode succesvol werd uitgevoerd, zal de `readyState` eigenschap de waarde `1` bevatten. Dit betekent dat er een connectie werd geopend naar de server `OPENED`.
- Nadat de `send` methode succesvol werd uitgevoerd en de `headers` van het antwoord ontvangen werden, zal de `readyState` eigenschap de waarde `2` bevatten `HEADERS_RECEIVED`.
- Wanneer de inhoud van het antwoord start met laden, zal de `readyState` eigenschap de waarde `3` bevatten `LOADING`.
- Wanneer alle inhoud van het antwoord volledig geladen is, zal de `readyState` eigenschap de waarde `4` bevatten `DONE`.

```javascript
 //1. Create a XMLHttpRequest object (Send to and load data from a WebAPI)
var xhr = typeof XMLHttpRequest != undefined
    ? new XMLHttpRequest()
    : new ActiveXObject('Microsoft.XMLHTTP');
//2. Declare the type of the response
xhr.responseType = 'json';
//3. Listen to the changes in states within the connection
xhr.onreadystatechange = function(){
    switch(xhr.readyState){
        case 0:console.log('UNSENT');break;
        case 1:console.log('OPENED');break;
        case 2:console.log('HEADERS RECEIVED');console.log(this.getAllResponseHeaders());break;
        case 3:console.log('LOADING');break;
        case 4:default:
            console.log('LOADED');
            //If status equals 200 then everything is ok else nok
            if(xhr.status == 200){
                console.log('OK');
                //Get the received data --> response
                var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
                console.log(data);
            }else {
                console.log(Error(xhr.status));
            }
            break;
    }
};
//4. Open the connection or tunnel to the resource on the url
xhr.open('GET', url, true);
//5. Make the request to the specified resource
xhr.send(null);
```
Uiteraard moeten we de `onreadystatechange`  luisteraar koppelen aan het `XMLHttpRequest` object om te kunnen luisteren naar de wijzigingen in de status van de connectie. Via de `abort` methode kunnen we, voordat de `readyState` eigenschap de waarde `4` bereikt, de afhandelingen annuleren. Dit wordt vooral gebruikt om dezelfde aanvragen te vermijden.

Indien de `status` eigenschap de waarde `200` bevat, nadat de `readyState` eigenschap de waarde `4` bereikt, zal alles succesvol verlopen zijn volgens de ingestelde eigenschappen. Indien dit niet het geval zou zijn, dan zal de applicatie niet verder kunnen interpreteren en moeten we dit kenbaar maken aan de gebruiker.

Indien we in het bovenstaande voorbeeld ook de type van het antwoord definiëren via de eigenschap `responseType` met als waarde `json`, dan zal het `XMLHttpRequest` object de ontvangen data automatisch converteren in de `response` eigenschap. Indien we dit niet doen, dan moeten we de `response` eigenschap zelf converteren naar JSON via de `JSON.parse(data)` methode. De `data` parameter bevat de waarde van het antwoord. De `responseText` eigenschap bevat de tekstuele, lees string, waarde van het antwoord.

Met de methode `getAllResponseHeaders()` kunnen we tijdens de `readyState` eigenschap met de waarde `2` all ontvangen Response Headers opvragen. Individuele headers kunnen opgevraagd worden met de `getResponseHeader(type)` methode opgevraagd worden, waarbij de paramter type, het type response header bevat, bv.: `getResponseHeader("Content-Type");`. 

> `XMLHttpRequest` met URL: http://datatank.gent.be/MilieuEnNatuur/Ecoplan.json
> 
![Resultaat van de bovenstaande code](https://lh4.googleusercontent.com/-qMLtJ8MdwEg/VFUjIb7NWwI/AAAAAAAAAl4/RM7lWceqEzo/s0/xmlhttprequest_1.PNG "xmlhttprequest_1.PNG")

Meestal wordt enkel de `readyState` eigenschap met waarde `4` aangesproken, de anderen niet. Maar ze kunnen wel nuttig zijn, vooral de waarde `1` om een **progress indicator** te visualiseren, en vervolgens te verbergen als we de waarde `4` bereiken.

###XMLHttpRequest luisteraars

Het `XMLHttpRequest` object laat toe om te luisteren naar verschillende gebeurtenissen die kunnen optreden tijdens het verwerken van de aanvraag.

De gebeurtenissen:

- `progress`  
Progressie van de verzendingen tussen de cliënt en de server, zowel upload als download.
- `load`  
Het antwoord is succesvol geladen.
- `error`  
Het optreden van een fout tijdens de transactie.
- `abort`  
De gebruiker heeft de transactie onderbroken.
- `loadend`  
Het antwoord is al dan niet succesvol geladen.

```javascript
var url = 'http://datatank.gent.be/MilieuEnNatuur/Ecoplan.json';
//1. Create a XMLHttpRequest object (Send to and load data from a WebAPI)
var xhr = typeof XMLHttpRequest != undefined
    ? new XMLHttpRequest()
    : new ActiveXObject('Microsoft.XMLHTTP');
//2. Declare the type of the response
xhr.responseType = 'json';
//3. Listen to various events
xhr.addEventListener("progress", updateProgress, false);
xhr.addEventListener("load", transferCompleted, false);
xhr.addEventListener("error", transferFailed, false);
xhr.addEventListener("abort", transferCanceled, false);
//4. Define the Event Listeners Methods
function updateProgress (ev) {
    if (ev.lengthComputable) {
        var percentComplete = ev.loaded / ev.total;
        console.log(percentComplete + '%');
    } else {
        // Unable to compute progress information since the total size is unknown
    }
}
function transferCompleted(ev) {
    console.log("The transfer is complete.");
    if(xhr.status == 200){
        //Get the received data --> response
        var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
        console.log(data);
    }
}
function transferFailed(ev) {
    console.log("An error occurred while transferring the file.");
    console.log(ev);
}
function transferCanceled(ev) {
    console.log("The transfer has been canceled by the user.");
}
//5. Open the connection or tunnel to the resource on the url
xhr.open('GET', url, true);
//6. Make the request to the specified resource
xhr.send(null);
```

###XMLHttpRequest met handlers

```javascript
function getJSON(url, successHandler, errorHandler){
        var xhr = typeof XMLHttpRequest != 'undefined'
            ? new XMLHttpRequest()
            : new ActiveXObject('Microsoft.XMLHTTP');

        xhr.open('get', url, true);
        xhr.responseType = 'json';
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4){
                if (xhr.status == 200) {
                    var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
                    successHandler && successHandler(data);
                } else {
                    errorHandler && errorHandler(status);
                }
            }
        };
        xhr.send();
    }

getJSON(url,
    function(data) {
        console.log(data);
    },
    function(status) {
        console.log(status);
    }
);
```

###XMLHttpRequest met Promise

```javascript
function getJSONByPromise(url){
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', url, true);
        xhr.responseType = 'json';
        xhr.onload = function() {
            if (xhr.status == 200) {
                var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
                resolve(data);
            } else {
                reject(status);
            }
        };
        xhr.onerror = function() {
            reject(Error("Network Error"));
        };
        xhr.send();
    });
}

getJSONByPromise(url).then(
    function(data) {
        console.log(data);
    },
    function(status) {
        console.log(status);
    }
);
```

Structuur van een front-end webapplicatie
-----------------------------------------

* project (folder)
    * `.sass-cache`
    * `.tmp`
    * `app`
    * `bower_components`
    * `dist`
    * `docs`
    * `node_modules`

###Bower

Indien npm (Node Package Manager) achter een proxy server wordt uitgevoerd, moeten we deze toevoegen! Zie document frontend_automationtools.md!

Bower kunnen we installeren via: `npm install -g bower`.

Na deze installatie moeten we een configuratiebestand aanmaken van bower:

<http://bower.io/docs/config/>

```
{
    "directory": "bower_components/",
    "analytics": false,
    "proxy":"",
    "https-proxy":"",
    "timeout":10000,
    "strict-ssl":false,
    "ca":null,
    "colors":true,
    "interactive":null
}
```

Aanmaak van een `bower.json` bestand via:

`bower init`

```
{
  "name": "Cocoa To-do Application",
  "version": "0.1",
  "authors": [
    "drdynscript <drdynscript@gmail.com>"
  ],
  "description": "To-do application for personal management of to-do lists via todo's, lists or categories, priorities, reminders, ...",
  "keywords": [
    "arteveldehogeschool",
    "arteveldehs",
    "2mmp",
    "cocoa",
    "todo",
    "application",
    "app",
    "html5",
    "css3"
  ],
  "license": "MIT",
  "homepage": "http://www.arteveldehogeschool.be",
  "private": true,
  "ignore": [
    "**/.*",
    "node_modules",
    "bower_components",
    "test"
  ]
}
```

`bower install --dev modernizr`
`bower install --dev crossroads.js`
`bower install --dev js-signals`
`bower install --dev hasher`
`bower install --dev jquery`

`npm install --save-dev gulp`

Installatie van alle bestanden vermeld in de `package.json` bestand.

`npm install`





Google Maps
-----------

* <https://developers.google.com/maps/>
* <https://developers.google.com/maps/documentation/javascript/>
* <https://code.google.com/apis/console/?noredirect&pli=1#project:769371415572>



Services
--------------

Een “service” is een applicatie die diensten levert aan andere applicaties in een netwerk, zoals bv. mobiele applicaties en/of websites. Een “web service” is een manier van communicatie tussen twee elektronische entiteiten over het WWW. Een service is een softwaresysteem ontworpen om uitwisselbare (interoprable) machine-naar-machine interacties over een netwerk te voorzien. Een webservice is een Service-oriented Architecture (SOA), waarbij XML en JSON worden vaak gebruikt worden als uitwisselingsformaten. XML en JSON worden vaak gebruikt als uitwisselingsformaten. Cloud computing is een afgeleide van SOA’s. De functionaliteiten binnen een service intrageren best niet met elkaar en dit om concurrency te voorkomen. Technologieën die ondersteund kunnen worden in webservices zijn:

* WSDL ("Web Service Description Language" - Beschrijft de beschikbare services)
, SOAP ("Simple Object Access Protocol" - Beschrijft de protocollen om te communiceren)
, RPC
* **REST**
* DCOM
* CORBA
* DDS
* Java RMI
* WCF

###REST services

REST staat voor "Representational State Transfer". Het is een architecturale stijl voor softwareapplicaties in een netwerk, dus nog geen standaard. Het laat verschillende softwarearchitecturen toe. Het werd voor het eerst geïntroduceerd door Roy T. Fielding in zijn thesis "Architectural Styles and the Design of Network-based Software Architectures". Het is bij uitstek de voornaamste architecturale stijl voor online services, omwille van de minder grote- en minder complexe aanvragen (requests).

Het WWW bestaat uit entiteiten (resources). Een entiteit wordt beschikbaar gesteld via een URL, bv.: http://www.arteveldehs.be/opleidingen/gdm. Zo’n entiteit kan een enkele entiteit, collectie of eender wat zijn, als het maar een object is. Het bezoeken van zo’n URL geeft de representatie van zo’n entiteit terug, waardoor de cliënt in een bepaalde status (state) verkeerd. Klikt de gebruiker op een aanwezige link binnen de voorgaande representatie, dan krijgt deze cliënt een nieuwe representatie en verkeerd dus hierdoor in een nieuwe status!

REST gebruikt de standaarden:

- HTTP(S)
- URL
- XML, (X)HTML, GIF, JPEG, JSON(P), … (Resources)
- Mime types: text/xml, text/html, image/gif, image/jpeg, …

De architecturale elementen binnen een REST service:

- Resource
Elke data die je kan benoemen, vergelijkbaar met een entiteit of een verzameling van entiteiten
- Resource Identifier
Meestal URI (Uniform Resource Identifier): URL (Uniform Resource Locator) + URN (Uniform Resource Name)
- Resource Metadata
Link naar de bron, alternatieven en varianten
- Representation
Een document (XML, (X)HTML, JSON(P), …)
- Representation Metadata
Mediatype (text/xml, text/html, …)

![REST communicatie](https://lh5.googleusercontent.com/-3CeIXE2wWPk/VFNgNtyfoBI/AAAAAAAAAlk/oTOnazOr-WA/s0/REST.png "REST.png")

> Een cliënt doet een aanvraag naar een resource via de resource identifier of URI. De controller, die verbonden is aan deze URI, verwerkt de aanvraag en haalt via de Service laag de noodzakelijke data op. Deze data wordt vervolgens in de controller geconverteerd of getransformeerd naar het aangevraagde formaat. Tenslotte wordt deze data verstuurd naar de cliënt die deze data zal verwerken via scripts.

###RESTful

REST is een abstracte architecturale stijl die toepasbaar is in vele technologieën.  Als men de principes van REST toepast in combinatie met HTTP (HyperText Transfer Protocol), dan spreekt men over RESTful HTTP of kortweg RESTful. 

De RESTful URL's identificeren de dingen waarop we willen inwerken. Een URL identificeert een resource:
 
- `/students`
Identificatie van alle studenten
- `/students/phildp`  
Identificeert een student, genaamd “phildp”
In dit geval moet de identificatie uniek zijn, vandaar dat we hier de gebruikersnaam gebruiken
- De “resource identifier” moet uniek zijn over het gehele WWW
- Aanvraag (request) van een resource naar een host  
`GET /students/phildp HTTP/1.1`, Host: `arteveldehs.be`

In RESTful beschrijven we het "resource path" m.b.v. zelfstandige naamwoorden (nouns) en dus niet via het omschrijven van acties:  `/students/add`. Dit is dus een ongeldig “resource path” in RESTful applicaties.
De URLs moeten zo precies mogelijk omschreven worden, alles wat de resource uniek maakt zou in de URL aanwezig moeten zijn, zoals bv.: primaire sleutels, gebruikersnamen, … .

Bij iedere aanvraag of "request" specificeren we een bepaalde HTTP verb of methode in de "request header". Deze HTTP method is het eerste woord, in hoofdletters, in de "request header". De HTTP methoden vertellen de “server” wat deze moet doen met de data geïdentificeerd door de URL. De "request" kan daarnaast ook data bevatten die mee verstuurd zal worden. Deze data brengen we onder in de "request body".

|Databasebewerkingen|SQL keyword|HTTP methods|Omschrijving|
|-------------------|-----------|------------|------------|
|Create|INSERT|POST|Aanmaak van een resource|
|Read|SELECT|GET|Lezen van de representatie van een resource|
|Update|UPDATE|PUT|Wijzigen van een resource|
|Delete|DROP|DELETE|Verwijderen van een resource|

De `GET` methode is de meest eenvoudige HTTP request methode. De browser gebruikt deze iedere keer wanneer een gebruiker klikt op een link of een URL typt in de adresbalk. Het geeft instructies aan de server om de representatie van de resource door te sturen naar de cliënt. Wordt voornamelijk gebruikt in Read-only of safe methods.

|HTTP methods|Omschrijving|
|-------------------|-----------|
|CONNECT|SSL connecties en proxy chaining|
|HEAD|Vergelijkbaar met GET, maar de representatie wordt niet teruggegeven enkel de HTTP header. Handig om bijvoorbeeld eerst na te gaan of een resource gewijzigd is, vooraleer we deze resource zullen downloaden via GET!|
|TRACE|Wordt gebruikt om bepaalde elementen te traceren tijdens de debug-fase. Meestal worden deze TRACE request uitgeschakeld door bepaalde condities te overschrijven!|







Bibliografie
--------------

> **XMLHttpRequest**
>
> - <http://www.w3schools.com/xml/xml_http.asp>
- <https://developer.mozilla.org/nl/docs/Web/API/XMLHttpRequest>
- <http://en.wikipedia.org/wiki/XMLHttpRequest>
- <https://developer.mozilla.org/nl/docs/Web/API/XMLHttpRequest/Using_XMLHttpRequest>
- <http://www.w3.org/TR/XMLHttpRequest/>
- <http://msdn.microsoft.com/en-us/library/ie/ms535874(v=vs.85).aspx>
- <https://xhr.spec.whatwg.org/>
- <https://developer.apple.com/library/iad/documentation/AppleApplications/Conceptual/SafariJSProgTopics/Articles/XHR.html>
- <http://en.wikipedia.org/wiki/List_of_HTTP_header_fields>


> **Weather API's**
> 
> - <https://developer.yahoo.com/yql/console/>
- <https://chrome.google.com/webstore/detail/postman-rest-client>
/fdmmgilgnpjigdojojpjoooidkmcomcm/related
- <https://weather.yahoo.com/forecast/BEXX0008_c.html>
- <http://woeid.rosselliot.co.nz/lookup/>

> **Broser plugins**
>
> - <https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm/related>
- <https://developer.chrome.com/extensions/xhr>