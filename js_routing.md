﻿JavaScript Routing
=============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Proof of concept
----------------

```javascript
(function(){
    //Variables for crossroads and hasher
    var router, hash;

    // Initialize the application
    function init(){
        router = crossroads;//Clone the crossroads object
        hash = hasher;//Clone the hasher object

        //Crossroads settings
        var sectionRoute = router.addRoute('/{section}');//Add the section route to crossroads
        sectionRoute.matched.add(onSectionMatch);//Hash matches to section route
        router.routed.add(console.log, console);//Log all crossroads events

        //Hash settings
        hash.initialized.add(onParseHash);//Parse initial hash
        hash.changed.add(onParseHash);//Parse changes in the hash
        hash.init();//Start listening to the hashes
    }

    //Event Listener: listen to the changes in th hash
    function onParseHash(newHash, oldHash){
        console.log('New: ' + newHash + ' <- Old: ' + oldHash);
        router.parse(newHash);
    }

    //Event Listener: listen to matched routes (parse)
    function onSectionMatch(section){
        console.log(section);
    }

    init();//Call the init function
})();
```

###Oplossen van home route problemen

```javascript
(function(){
    //Variables for crossroads and hasher
    var router, hash;

    // Initialize the application
    function init(){
        ...

        //Crossroads settings
        var homeRoute = router.addRoute('/',function(){onSectionMatch('home');});
        ...
    }

    ...
})();
```

###Pagina secties activeren of deactiveren

```javascript
(function(){
    ...

    //Event Listener: listen to matched routes (parse)
    function onSectionMatch(section){
        //console.log(section);
        var pages = document.querySelectorAll('.page');
        if(pages != null && pages.length > 0){
            _.each(pages,function(page){
               if(page.id == section){
                   page.classList.add('active');
               }else{
                   page.classList.remove('active');
               }
            });
        }
    }

    ...
})();
```


Bibliografie
------------

> **JS-Signals**
>
- <http://millermedeiros.github.io/js-signals/>
>
> **Crossroads.js**
>
- <http://millermedeiros.github.io/crossroads.js/>
>
> **Hasher**
>
- <https://github.com/millermedeiros/hasher/>












