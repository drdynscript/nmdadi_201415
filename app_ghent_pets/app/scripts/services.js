/**
 * Created by Administrator on 22/11/14.
 * Dierenartsen: http://data.appsforghent.be/poi/dierenartsen.json
 * Hondentoiletten: http://data.appsforghent.be/poi/hondentoiletten.json
 * Hondenvoorzieningen: http://datatank.gent.be/Infrastructuur/Hondenvoorzieningen.json
 *
 */

var DierenartsenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getDierenartsen:function(amount){
        var url = this.connString;
        return Utils.getJSONByPromise(url);
    }
};

var HondentoilettenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getHondentoiletten:function(amount){
        var url = this.connString;
        return Utils.getJSONByPromise(url);
    }
};

var HondenvoorzieningenAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getHondenvoorzieningen:function(amount){
        var url = this.connString;
        return Utils.getJSONByPromise(url);
    }
};

var PetsDBContext = {
    init:function(connString){
        this.connString = connString;
        //Create the AppData object for the Users Application
        this.AppData = {
            "information":{
                "title":"Pets Application Ghent",
                "version":"1.0",
                "modified":"22-11-2014",
                "author":"Philippe De Pauw - Waterschoot"
            },
            "vets":[],
            "dogstoilets":[],
            "dogsrunningplaces":[],
            "parks":[],
            "settings":{}
        };
        //Get Application Data from the localstorage
        if(Utils.store(this.connString) != null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
    getVets:function(){
        var vets = this.AppData.vets;

        if(vets == null || (vets != null && vets.length == 0))
            return null;

        vets = _.sortBy(vets,'naam');
        return {"vets":vets};
    },
    getDogsRunnigPlaces:function(){
        var dogsrunningplaces = this.AppData.dogsrunningplaces;

        if(dogsrunningplaces == null || (dogsrunningplaces != null && dogsrunningplaces.length == 0))
            return null;

        dogsrunningplaces = _.sortBy(dogsrunningplaces,'guid');
        return {"dogsrunningplaces":dogsrunningplaces};
    },
    getDogsToilets:function(){
        var dogstoilets = this.AppData.dogstoilets;

        if(dogstoilets == null || (dogstoilets != null && dogstoilets.length == 0))
            return null;

        dogstoilets = _.sortBy(dogstoilets,'guid');
        return {"dogstoilets":dogstoilets};
    },
    getVetById:function(id){
        var vet = _.find(this.AppData.vets, function(vet){
            return (vet.guid == id);
        });//Find an entity by id

        if(typeof vet == 'undefined')
            return null;//Make null <-- undefined object

        return vet;
    },
    getDogsRunnigPlaceById:function(id){
        var place = _.find(this.AppData.dogsrunningplaces, function(place){
            return (place.guid == id);
        });//Find an entity by id

        if(typeof place == 'undefined')
            return null;//Make null <-- undefined object

        return place;
    },
    getDogsToiletById:function(id){
        var toilet = _.find(this.AppData.dogstoilets, function(toilet){
            return (toilet.guid == id);
        });//Find an entity by id

        if(typeof toilet == 'undefined')
            return null;//Make null <-- undefined object

        return toilet;
    },
    getAmountOfVets:function(){
        var vets = this.AppData.vets;

        if(vets == null || (vets != null && vets.length == 0))
            return 0;

        return vets.length;
    },
    getNearestPlaces:function(geoLocation){
        var places = [], nearestPlace = null, distance = 0, self = this;

        if(geoLocation == null)
            return null;

        _.each(this.AppData.vets, function(vet){
            distance = Utils.calculateDistanceBetweenTwoCoordinates(vet.lat, vet.long, geoLocation.coords.latitude, geoLocation.coords.longitude);

            if(nearestPlace == null){
                nearestPlace = self.createNearestPlace(vet.lat, vet.long, distance, "Dierenarts", vet.naam, vet.adres.capitalize()                 + " " + vet.huisnr + ", " + vet.postcode + " " + vet.gemeente.capitalize());
            }else if(nearestPlace != null && distance < nearestPlace.afstand){
                nearestPlace = self.createNearestPlace(vet.lat, vet.long, distance, "Dierenarts", vet.naam, vet.adres.capitalize()                 + " " + vet.huisnr + ", " + vet.postcode + " " + vet.gemeente.capitalize());
            }
        })

        places.push(nearestPlace);

        nearestPlace = null;

        _.each(this.AppData.dogstoilets, function(toilet){
            distance = Utils.calculateDistanceBetweenTwoCoordinates(toilet.lat, toilet.long, geoLocation.coords.latitude, geoLocation.coords.longitude)

            if(nearestPlace == null || (nearestPlace != null && distance < nearestPlace.afstand)){
                nearestPlace = self.createNearestPlace(toilet.lat, toilet.long, distance, "Hondentoilet", toilet.plaats, toilet.wijk_ronde);
            }
        });

        places.push(nearestPlace);

        nearestPlace = null;

        _.each(this.AppData.dogsrunningplaces, function(place){
            distance = Utils.calculateDistanceBetweenTwoCoordinates(place.lat, place.long, geoLocation.coords.latitude, geoLocation.coords.longitude)

            if(nearestPlace == null || (nearestPlace != null && distance < nearestPlace.afstand)){
                nearestPlace = self.createNearestPlace(place.lat, place.long, distance, "Losloopweiden", place.plaatsomsc, place.postcode + ' ' + place.gemeente);
            }
        });

        places.push(nearestPlace);

        return {"places":places};
    },
    createNearestPlace:function(lat, lng, afstand, type, naam, adres){
        return {
            "lat":lat,
            "lng":lng,
            "afstand":afstand,
            "type":type,
            "naam":naam,
            "adres":adres
        };
    },
    addVet:function(vet){

        vet.guid = Utils.guid();

        if(this.getVetById(vet.guid) != null)
            return 0;

        this.AppData.vets.push(vet);
        this.save();

        return 1;
    },
    addDogsRunningPlace:function(place){

        place.guid = Utils.guid();

        if(this.getDogsRunnigPlaceById(place.guid) != null)
            return 0;

        this.AppData.dogsrunningplaces.push(place);
        this.save();

        return 1;
    },
    addDogsToilet:function(toilet){

        toilet.guid = Utils.guid();

        if(this.getDogsToiletById(toilet.guid) != null)
            return 0;

        this.AppData.dogstoilets.push(toilet);
        this.save();

        return 1;
    },
    save:function(){
        this.AppData.modified = new Date().getTime();
        Utils.store(this.connString, this.AppData);//Save all data to the localstorage
    }
};

var GMap = {
    init:function(container){
        var mapOptions = {
            zoom:13,
            center: new google.maps.LatLng(51.048017, 3.727666)
        }
        this.map = new google.maps.Map(document.querySelector('#' + container), mapOptions);
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this.map,'resize');
        this.geoLocationMarker = null;
        this.markersVets = [];
        this.markersDogsRunningPlaces = [];
        this.markersDogsToilets = [];
    },
    addMarkerGeoLocation:function(geoLocation){
        this.geoLocationMarker = new google.maps.Marker({
            position:new google.maps.LatLng(geoLocation.coords.latitude, geoLocation.coords.longitude),
            title:"Hier ben ik",
            icon:'content/images/maps_home.png'
        });//Create a Google Maps Marker

        this.geoLocationMarker.setMap(this.map);//Add Marker to Map
        this.map.setCenter(new google.maps.LatLng(geoLocation.coords.latitude, geoLocation.coords.longitude));//Set center of the map to my geolocation
    },
    addMarkersForVets:function(vets){
        var marker = null, self = this;

        _.each(vets, function(vet){
            marker = new google.maps.Marker({
                position:new google.maps.LatLng(vet.lat, vet.long),
                title:vet.naam,
                icon:'content/images/maps_veterinary.png'
            });//Create a Google Maps Marker

            marker.setMap(self.map);//Add Marker to Map

            self.markersVets.push(marker);
        });
    },
    addMarkersForDogsRunningPlaces:function(places){
        var marker = null, self = this;

        _.each(places, function(place){
            marker = new google.maps.Marker({
                position:new google.maps.LatLng(place.lat, place.long),
                title:place.plaatsomsch,
                icon:'content/images/dogs_offleash.png'
            });//Create a Google Maps Marker

            marker.setMap(self.map);//Add Marker to Map

            self.markersDogsRunningPlaces.push(marker);
        });
    },
    addMarkersForDogsToilets:function(toilets){
        var marker = null, self = this;

        _.each(toilets, function(toilet){
            marker = new google.maps.Marker({
                position:new google.maps.LatLng(toilet.lat, toilet.long),
                title:toilet.wijk_ronde,
                icon:'content/images/maps_pets.png'
            });//Create a Google Maps Marker

            marker.setMap(self.map);//Add Marker to Map

            self.markersDogsToilets.push(marker);
        });
    },
    showMarkersForPage:function(page){
        console.log(page);
        switch(page){

            case 'vets':default:
                this.hideMarkers(this.markersVets, false);
                this.hideMarkers(this.markersDogsRunningPlaces,true);
                this.hideMarkers(this.markersDogsToilets,true);
                break;
            case 'dogstoilets':
                this.hideMarkers(this.markersVets, true);
                this.hideMarkers(this.markersDogsRunningPlaces,true);
                this.hideMarkers(this.markersDogsToilets,false);
            break;
            case 'runningplaces':
                this.hideMarkers(this.markersVets, true);
                this.hideMarkers(this.markersDogsRunningPlaces,false);
                this.hideMarkers(this.markersDogsToilets,true);
            break;
        }
    },
    hideMarkers:function(arrMarkers, hide){
        var self = this;

        _.each(arrMarkers, function(marker){
            if(hide){
                marker.setMap(null);
            }else{
                marker.setMap(self.map);
            }
        });
    },
    refresh:function(){
        google.maps.visualRefresh = true;
        google.maps.event.trigger(this.map,'resize');
    }
};

