CSS 3D
=============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

In CSS kunnen we CSS transformaties uitvoeren in de 3D-ruimte. Configuratie van zo'n 3D-ruimte gebeurt via de `perspective` eigenschap. Het gedrag van de 2D-elementen in de 3D-ruimte moeten we hierna configureren .

3D-translatie
----------------

Translatie of verschuiving van een element, in dit geval in de 3D-ruimte. Translatie wordt in CSS gerealiseerd via de translate methoden:

- `translate3d(tx,ty,tz) `
- `translateX(tx)`
Translatie langs de X-as
- `translateY(ty)`
Translatie langs de Y-as
- `translateZ(tz)`
Translatie langs de Z-as

De waarden van translatie (tx, ty, tz) kunnen uitgedrukt worden in een lengte (getal gevolgd door een lengte-eenheid, zoals: px, em, in, pt, mm, …) of in percentage (%).

3D-schalen
-------------

Elementen kunnen geschaald worden in de X- , Y-richting en/of Z-richting. Schalen wordt in CSS gerealiseerd via de scale methoden:

- `scale3d(sx,sy,sz) `
- `scaleX(sx)`
- `scaleY(sy)`
- `scaleZ(sz)`

De waarden van schalen (sx, sy, sz) moeten uitgedrukt worden met een getal (positief of negatief floating-point getal).

##Bibliografie

> **3D-transformaties**
>
> - http://www.w3schools.com/css/css3_3dtransforms.asp
