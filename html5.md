﻿HTML5
=============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Globale Attributen
----------------------

Globale attributen zijn attributen die op elk html-element kunnen toegepast worden.

Pré HTML5 globale attributen:

- `class`
Klasnamen voor een element.
- `id`  
Unieke id voor een element.
- `dir`  
Tekstrichting voor de inhoud van een element.
- `lang`  
Taalinstelling voor de inhoud van een element.
- `title`  
Extra informatie over het element.
- `acceskey`  
Een sneltoets om de focus op het element te leggen.
- `tabindex`  
Tab-volgorde van een element.
- `style`  
Inline CSS voor een element. **NOT DONE**. Wel toepasbaar voor HTML email nieuwsbrieven.

**Nieuwe globale attributen in HTML5**:

- `contenteditable`
De inhoud van een element is al dan niet editeerbaar.
- `contextmenu`
De inhoud van een element is al dan niet editeerbaar.
- `spellcheck`
Al dan niet bekijken van de spelling en de grammatica van de inhoud van het element.
- `translate`
Al dan niet vertalen van de inhoud van het element.
- `hidden`
Specificeren of een element nog niet of niet meer relevant is.
- `data-*`
Bewaren van specifieke data van een element in de webpagina of applicatie.
- `draggable`
Specificeren of een element al dan niet versleepbaar is.
- `dropzone`
Specificeren of de versleepte data wordt gekopieerd, verplaatst of gelinkt.

###Contenteditable

De inhoud van een element is al dan niet editeerbaar. Indien niet gespecificeerd zal de instelling van de ouder overgenomen worden. Dit inhoud wordt niet bewaard na het herladen van de webpagina. 

```
<element contenteditable="true|false">
``` 

![enter image description here](https://lh5.googleusercontent.com/-tM-F5tHhnzk/VC7FZcUtAcI/AAAAAAAAAbA/4UyDexhCUuY/s0/contenteditable.png "contenteditable.png")

###Contextmenu


```
<element spellcheck="true|false">
``` 

![enter image description here](https://lh4.googleusercontent.com/-JahJ4HO9Xzg/VC7Fjpe7bZI/AAAAAAAAAbM/qECuUPfmHAg/s0/contextmenu.png "contextmenu.png")

###Spellcheck


```
<element spellcheck="true|false">
``` 

![enter image description here](https://lh3.googleusercontent.com/-SylacVZ7hqg/VC7FUSEDiiI/AAAAAAAAAa0/9T35iy3Z9EA/s0/spellcheck.png "spellcheck.png")

###Translate

**Het `translate` attribuut is voorlopig in geen enkele moderne browser geïmplementeerd.**

###Hidden

Specificeren of een element nog niet of niet meer relevant is. Webbrowsers zullen dit element niet visueel tonen indien hidden gespecificeerd is. Door een bepaalde actie, bijvoorbeeld door een checkbox aan te vinken, kan dit element wel getoond worden indien we het `hidden` attribuut verwijderen.

```
<element hidden[="hidden"]>
``` 

###Draggable

```
<element draggable="true|false">
```

Afbeeldingen en hyperlinks zijn standaard versleepbaar. Natuurlijk enkel als het `src` attribuut (bij een afbeeldingen) en het `href` attribuut gespecificeerd zijn.

|Draggable image|Draggable link|
|---------------|--------------|
|![enter image description here](https://lh4.googleusercontent.com/-CDLM1uPzjOY/VC7N4BW1R8I/AAAAAAAAAbw/sN3l7q48urE/s0/draggable_image.png "draggable_image.png")|![enter image description here](https://lh4.googleusercontent.com/-Vq3j5tKabkU/VC7N8_8udSI/AAAAAAAAAb8/XnO-kbNAdQw/s0/draggable_link.png "draggable_link.png")|

Wanneer we andere HTML-elementen wensen te verslepen, moeten we aan het attribuut `draggable` de waarde `true` toekennen.

**Drag-and-Drop (DnD)** werkt enkel indien we:

- Elementen versleepbaar maken.
- Een element hebben waarin we deze versleepbare elementen kunnen droppen.
- JavaScript event handlers specificeren op het voorgaande element, die vertellen dat op dit element elementen kunnen gedropt worden.

####Drag Events

Verschillende gebeurtenissen of events worden afgevuurd tijdens een drag-and-drop operatie. Enkgel drag-events worden afgevuurd dus niet de muis-events, zoals `mousemove`, ... . De gebeurtenissen `dragstart` en `dragend` worden enkel binnenin een webbrowser afgevuurd. Dus niet wanneer we een bestand buiten de webbrowser verslepen in een webbrowser.

- `dragstart`  
Wordt afgevuurd wanneer het verslepen gestart is. Tijdens deze gebeurtenis kan de gebruiker de data specificeren alsook een afbeelding als visuele feedback.
- `dragend`  
Wordt afgevuurd wanneer het verslepen voltooid is. Het verslepen kan al dan niet succesvol verlopen zijn.
- `dragenter`  
Wordt afgevuurd wanneer de muis de dropzone betreedt tijdens het verslepen. Visuele feedback is aangewezen tijdens deze gebeurtenis.
- `dragover`  
Wordt afgevuurd wanneer de muis in de dropzone zich verplaatst tijdens het verslepen. 
- `dragleave`  
Wordt afgevuurd wanneer de muis de dropzone verlaat tijdens het verslepen. Visuele feedback is aangewezen tijdens deze gebeurtenis.
- `drop`  
Wordt afgevuurd op de dropzone op het einde van het verslepen (enkel indien de muis zich binnen deze dropzone bevindt). Binnen deze luisteraar halen we de data op die tijdens de start van het verslepen werd gespecificeerd.

```
...
<section id="example5" class="example">
    <h1>Draggable</h1>
    <div class="dropzone"></div>
    <img draggable="true" src="http://lorempixel.com/400/200/nature/1/" alt="Ipsum-afbeelding" />
    <br />
    <a draggable="true" href="http://www.arteveldehogeschool.be" title="Arteveldehogeschool website">Website Arteveldehogeschool</a>
    <blockquote draggable="true">
        Set the data for a given type. If data for the type does not exist, it is added at the end, such that the last item in the types list will be the new format. If data for the type already exists, the existing data is replaced in the same position. That is, the order of the types list is not changed when replacing data of the same type.
    </blockquote>
</section>
...
```

```
/*
Created by: Philippe De Pauw - Waterschoot
Date: 04-10-2014
Name: app.js
Description: JavaScript for the application
*/
(function(){
    var dropzones = document.querySelector('.dropzone');
    dropzones.addEventListener('dragenter', dragEnter, false);
    dropzones.addEventListener('dragover', dragOver, false);
    dropzones.addEventListener('dragleave', dragLeave, false);
    dropzones.addEventListener('drop', dragDrop, false);

    var draggables = document.querySelectorAll('[draggable="true"]');
    if(draggables != null){
        for (var i = 0; i < draggables.length; i++) {
            draggables[i].addEventListener('dragstart', dragStart, true);
            draggables[i].addEventListener('drag', drag, true);
            draggables[i].addEventListener('dragend', dragEnd, true);
        }
    }

    function dragEnter(ev) {
        ev.preventDefault();

        console.log('DRAG ENTER');

        return false;
    }

    function dragOver(ev) {
        ev.preventDefault();

        //console.log('DRAG OVER');//Wordt afgevoerd bij elke muisverplaatsing!

        return false;
    }

    function dragLeave(ev) {
        ev.preventDefault();

        console.log('DRAG LEAVE');

        return false;
    }

    function dragDrop(ev) {
        ev.preventDefault();

        console.log('DRAG DROP');

        return false;
    }

    function dragStart(ev) {
        console.log('DRAG START');

        return true;
    }

    function drag(ev) {
        //console.log('DRAG');//Wordt afgevoerd bij elke muisverplaatsing!

        return true;
    }

    function dragEnd(ev) {
        console.log('DRAG END');

        return true;
    }
})();
```

![enter image description here](https://lh4.googleusercontent.com/-LBo5DxJ-wik/VDGNfMoCj9I/AAAAAAAAAcY/xjhge3lfWXA/s0/drag_console.PNG "drag_console.PNG")

We hebben bij de `drag` en `dragover` gebeurtenissen bewust geen `console.log(string)` methode gebruikt omdat deze gebeurtenissen bij iedere muisverplaatsing worden afgevuurd. De volgorde van de gebeurtenissen:

> DRAG START \> DRAG ENTER \> DRAG DROP \> DRAG END

####Visuele feedback

```html
<section id="example1" class="example">
    <h1>Draggable</h1>
    <div class="dropzone"></div>
    <img draggable="true" src="http://lorempixel.com/400/200/nature/1/" alt="Ipsum-afbeelding" />
    <br />
    <a draggable="true" href="http://www.arteveldehogeschool.be" title="Arteveldehogeschool website">Website Arteveldehogeschool</a>
    <blockquote draggable="true">
        Set the data for a given type. If data for the type does not exist, it is added at the end, such that the last item in the types list will be the new format. If data for the type already exists, the existing data is replaced in the same position. That is, the order of the types list is not changed when replacing data of the same type.
    </blockquote>
</section>
```

```css
/*
Dropzone
*/
.dropzone{
    height:248px;
    background:rgba(33,33,33,0.86);
    border:2px dotted rgba(187,204,0,0.86);
    margin-bottom:24px;
    -webkit-transition:none;
}
.dropzone.dragenter{
    border-color:rgba(33,33,33,0.86);
    background-color:rgba(187,204,0,0.86);
}
.dropzone.dragdrop{
    border-color:rgba(33,33,33,0.86);
    background:rgba(238,153,0,0.86);
    transition:all 2s ease-out 0s;
}
```

```js
var dragMode = 'copy';

(function(){
    var dropzones = document.querySelector('.dropzone');
    if(dropzones != null){
        dropzones.addEventListener('dragenter', dragEnter, false);
        dropzones.addEventListener('dragover', dragOver, false);
        dropzones.addEventListener('dragleave', dragLeave, false);
        dropzones.addEventListener('drop', dragDrop, false);
    }

    var dropzonesDrop = document.querySelector('.dragdrop');
    if(dropzonesDrop != null){
        dropzonesDrop.addEventListener("transitionend", endOfDrop, false);
        dropzonesDrop.addEventListener("webkitTransitionEnd", endOfDrop, false);
        dropzonesDrop.addEventListener("mozTransitionEnd", endOfDrop, false);
        dropzonesDrop.addEventListener("msTransitionEnd", endOfDrop, false);
        dropzonesDrop.addEventListener("oTransitionEnd", endOfDrop, false);
    }

    function dragEnter(ev) {
        ev.preventDefault();

        this.classList.toggle('dragenter');
        ev.dataTransfer.dropEffect = "copy";

        return false;
    }

    function dragOver(ev) {
        ev.preventDefault();

        ev.dataTransfer.dropEffect = "copy";

        return false;
    }

    function dragLeave(ev) {
        ev.preventDefault();

        this.classList.toggle('dragenter');

        return false;
    }

    function dragDrop(ev) {
        ev.preventDefault();

        this.classList.toggle('dragenter');
        this.classList.toggle('dragdrop');

        var dropzonesDrop = document.querySelector('.dragdrop');
        if(dropzonesDrop != null){
            dropzonesDrop.addEventListener("transitionend", endOfDrop, false);
            dropzonesDrop.addEventListener("webkitTransitionEnd", endOfDrop, false);
            dropzonesDrop.addEventListener("mozTransitionEnd", endOfDrop, false);
            dropzonesDrop.addEventListener("msTransitionEnd", endOfDrop, false);
            dropzonesDrop.addEventListener("oTransitionEnd", endOfDrop, false);
        }

        return false;
    }

    function dragStart(ev) {

        ev.dataTransfer.effectAllowed = 'copyMove';

        return true;
    }

    function dragEnd(ev) {

        return true;
    }

    function endOfDrop(ev) {

        this.classList.toggle('dragdrop');

        return true;
    }
})();
```


###Dropzone

**Het `dropzone` attribuut is voorlopig in geen enkele moderne browser geïmplementeerd.**

```
<element dropzone="copy|move|link">
``` 

- `copy`  
Kopij van de versleepte data.
- `move`  
De versleepte data wordt verplaatst naar de nieuwe locatie.
- `link`  
De versleepte data wordt gelinkt in deze nieuwe locatie.

HTML Forms
---------------

###Nieuwe inputtypen

####Color

####Date

####Datetime

####Email

####Month

####Number

####Range

####Search

####Tel

####Week

###Nieuwe formulierelementen

####Datalist

Het `<datalist>` element definieert een lijst van voorgedefinieerde opties voor een `<input>` element. De functionaliteit **"autocomplete"** wordt hierdoor gerealiseerd. Het `<input>` element wordt verbonden met een `<datalist>` via het `list="id van de datalist"` attribuut. Een `<datalist>` element wordt niet ondersteund in IE9 of lager.

```
<form method="get" action="#">
    <div>
        Selecteer jouw favoriete webbrowser:
        <input list="webbrowsers" name="favowebbrowser">
        <datalist id="webbrowsers">
            <option value="Chrome">
            <option value="Firefox">
            <option value="Internet Explorer">
            <option value="Opera">
            <option value="Safari">
        </datalist>
    </div>
    <input type="submit" value="Verzenden">
</form>
```

![HTML Form Datalist](https://lh4.googleusercontent.com/-T2owjVHkUW8/VDfTamKSN4I/AAAAAAAAAjk/vmifUDXGKss/s0/datalist.png "datalist.png")

> `index.html?favowebbrowser=Internet+Explorer#`

####Keygen

Het `<keygen>` element wordt gebruikt om op een veilige manier gebruikers te authentificeren. Wanneer het formulier verzonden wordt, zullen er twee sleutels gegenereerd worden, namelijk een privésleutel en een publieke sleutel. De publieke sleutel wordt verstuurd naar de server. `<keygen>` wordt niet ondersteund in IE10 of lager.

Het `<keygen>` element bevat een aantal instelbare eigenschappen via de corresponderende attributen:

- `keytype="type van de sleutel"`  
Mogelijke waarden: `RSA`, `DSA` of `EC`
- `keyparams="type van keyparams"`  
Mogelijke waarden: `pqg-params` of `pqg-params`
- `challange="string"`  

```
<form method="get" action="#">
   <div>
       Gebruikersnaam:
       <input name="userName">
   </div>
   <div>
       Encryptie:
       <keygen name="security" />
   </div>
   <input type="submit" value="Verzenden">
</form>
```

![HTML keygen element](https://lh6.googleusercontent.com/-FAcQH70rteM/VDfXi9nb_iI/AAAAAAAAAkA/ROVezauJlh0/s0/keygen.PNG "keygen.PNG")

> `index.html?userName=drdynscript&security=MIICQDCCASg...`

####Output

Het `<output>` element representeert het resultaat van een wiskundige berekening.

```
<form method="get" action="#" oninput="x.value=parseInt(a.value)*parseInt(b.value)">
    <div>
        0
        <input type="range"  id="a" name="a" value="10" min="0" max="20">
        20 *
        <input type="number" id="b" name="b" value="5" min="0" max="8">
        =
        <output name="x" for="a b"></output>
    </div>
    <input type="submit" value="Verzenden">
</form>
```

In het `oninput` attribuut van het `<form>` element definiëren we de wiskundige berekeningen. De variabelen in deze berekeningen worden gedefinieerd door het `<output>` element via de `<name>` en `<for>` attributen. De variabelen `a` en `b` zijn afkomstig uit de corresponderende `<input>` elementen. De code `parseInt(a.value)` kunnen we in HTML5 vervangen door `a.valueAsNumber`.

![HTML output element](https://lh5.googleusercontent.com/-pzd8SoAWE6Y/VDfXZqOnPII/AAAAAAAAAj0/fnS-freWtmU/s0/output.PNG "output.PNG")

```
<section class="example">
    <h1>Range Input Type With Output</h1>
    <p>Niet ondersteund in IE9</p>
    <form oninput="x.value = nmdadi.value">
        <div>
            Punten opdracht nmdadi:
            <input type="range" name="nmdadi" id="nmdadi" min="0" max="20">
            <output name="x" for="nmdadi">10</output>
        </div>
        <input type="submit" value="Verzenden">
    </form>
</section>
```

![Range With Output](https://lh5.googleusercontent.com/-h6DhijG0t3w/VEUBhN3Hw2I/AAAAAAAAAk4/pz1IF3Z6_zM/s0/rangewithoutput.PNG "Range With Output")


###Nieuwe formulierattributen


###Nieuwe inputtypen




##Bibliografie

> **Globale attributen**
>
> - <http://www.w3schools.com/css/css3_3dtransforms.asp>
- <http://html5doctor.com/the-contenteditable-attribute/>
- <http://www.w3schools.com/html/html5_draganddrop.asp>
- <http://www.html5rocks.com/en/tutorials/dnd/basics/>
- <http://html5doctor.com/native-drag-and-drop/>
- <https://developer.mozilla.org/en-US/docs/Web/API/DataTransfer>
> 
> **Transition**
>   
>   - <https://developer.mozilla.org/en-US/docs/Web/Events/transitionend>


