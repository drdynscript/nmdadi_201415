/**
 * Created by drdynscript on 23/09/14.
 */
(function(){
    var element = document.querySelector("#navigation-toggle");
    element.addEventListener("click", toggleNavigation, false);
    function toggleNavigation(ev) {
        ev.preventDefault();

        var navigation_reference_name = this.getAttribute("href");
        var navigation = document.querySelector(navigation_reference_name);
        navigation.classList.toggle('open');

        this.classList.toggle('open');

        return false;
    }
})();
