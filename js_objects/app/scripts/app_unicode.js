/*
Created by: Philippe De Pauw - Waterschoot
Date: 15-10-2014
Name: app_unicode.js
Description: JavaScript for the application
*/
(function(){
    var _unicodeContainer = document.querySelector('#unicode-list');
    var _animatedFrom = 0, _animatedAmount = 0, _currentAnimatedFrom = 0;

    function createUnicodeItems(from, amount){
        for(var i=from;i<(from+amount);i++){
            createUnicodeItem(i);
        }
    }

    function createUnicodeItem(charCodeNumber){
        var c = String.fromCharCode(charCodeNumber);
        var htmlContent = '<div>'
                + c
                + '<span>'
                + charCodeNumber
                + '</span>'
                + '</div>';
        _unicodeContainer.innerHTML += htmlContent;
    }

    function createUnicodeItemsAnimated(from, amount){
        _animatedAmount = amount;
        _animatedFrom = _currentAnimatedFrom = from;
        window.setTimeout(function(){createUnicodeItemAnimated();}, 5);
    }

    function createUnicodeItemAnimated(){
        createUnicodeItem(_currentAnimatedFrom);
        if(_currentAnimatedFrom + 1 < (_animatedFrom+_animatedAmount)){
            _currentAnimatedFrom++;
            window.setTimeout(function(){createUnicodeItemAnimated();}, 5);
        }
    }

    createUnicodeItemsAnimated(100,100);

})();