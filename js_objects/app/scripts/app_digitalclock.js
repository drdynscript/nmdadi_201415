/*
Created by: Philippe De Pauw - Waterschoot
Date: 15-10-2014
Name: app_unicode.js
Description: JavaScript for the application
*/
(function(){
    var _digitalclock = document.querySelector('.digitalclock');

    function startClock(){
        window.setTimeout(function(){tickClock();}, 10);
    }

    function tickClock(){
        var date = new Date();
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        var ms = date.getMilliseconds();

        _digitalclock.innerHTML = prefixWithDigits(h,2,'0')
        + ':' + prefixWithDigits(m,2,'0')
        + ':' + prefixWithDigits(s,2,'0')
        + ' ' + prefixWithDigits(ms,3,'0')
        window.setTimeout(function(){tickClock();}, 10);

        _digitalclock.style['-webkit-transform'] = 'rotate(' + s*6 + 'deg) scale(' + (1+ms/1000) + ')';
        _digitalclock.style['-webkit-transform-origin'] = '50% 50%';
        var v = 255-Math.floor(ms/1000*255);
        var b = 255-Math.floor(s/60*255);
        _digitalclock.style['color'] = 'rgba(' + v + ', ' + (255-v) + ', ' + b + ',0.86)';
    }

    function prefixWithDigits(str, n, symb){
        if(typeof str !== 'string'){
            str = str.toString();
        }
        while(str.length < n){
            str = symb + str;
        }
        return str;
    }

    startClock();
})();