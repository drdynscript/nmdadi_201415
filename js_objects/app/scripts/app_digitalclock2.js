/*
Created by: Philippe De Pauw - Waterschoot
Date: 15-10-2014
Name: app_unicode.js
Description: JavaScript for the application
*/
(function(){
    var _digitalclock = document.querySelector('.digitalclock');

    function startClock(){
        window.setTimeout(function(){tickClock();}, 200);
    }

    function tickClock(){
        var date = new Date();
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();

        var hd = prefixWithDigits(h,2,'0');
        var md = prefixWithDigits(m,2,'0');
        var sd = prefixWithDigits(s,2,'0');

        _digitalclock.querySelector('.digit:nth-of-type(1)').innerHTML = hd[0];
        _digitalclock.querySelector('.digit:nth-of-type(2)').innerHTML = hd[1];
        _digitalclock.querySelector('.digit:nth-of-type(4)').innerHTML = md[0];
        _digitalclock.querySelector('.digit:nth-of-type(5)').innerHTML = md[1];
        _digitalclock.querySelector('.digit:nth-of-type(7)').innerHTML = sd[0];
        _digitalclock.querySelector('.digit:nth-of-type(8)').innerHTML = sd[1];

        window.setTimeout(function(){tickClock();}, 200);

        /*_digitalclock.style['-webkit-transform'] = 'rotate(' + s*6 + 'deg) scale(' + (1+ms/1000) + ')';
        _digitalclock.style['-webkit-transform-origin'] = '50% 50%';
        var v = 255-Math.floor(ms/1000*255);
        var b = 255-Math.floor(s/60*255);
        _digitalclock.style['color'] = 'rgba(' + v + ', ' + (255-v) + ', ' + b + ',0.86)';*/
    }

    function prefixWithDigits(str, n, symb){
        if(typeof str !== 'string'){
            str = str.toString();
        }
        while(str.length < n){
            str = symb + str;
        }
        return str;
    }

    startClock();
})();