CSS Selectoren
============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***


Element selectoren
-----------------------

|Selector|Voorbeeld|Omschrijving|
|--------|---------|------------|
|*       |`* `       |Selecteren van alle elementen|
|.class  |`.article-synopsis`|Selecteren van alle elementen met `class="article-synopsis`|
|#id     |`#canvas-2d`|Selecteren van het element met `id="canvas-2d"`|
|element|`section`|Selecteren van alle `<section>`-elementen|
|element,element|`section, footer`|Selecteren van alle `<section>` en `<footer>` elementen|
|element element|`section article`|Selecteren van alle `<article>` elementen binnen `<section>` elementen|
|element>element|`#menu>ul>li`|Selecteren van alle `<ul>` elementen waarvan de rechtstreekse ouder een id-attribuut bevat met als waarde `id="menu"`. Daarna selecteren we alle `<li>` elementen waarvan de rechtstreekse ouder in de lijst van geselecteerde `<ul>` elementen voorkomt.|
|element + element|`div + p`|Selecteert `<p>` elementen dat direct na de `<div>` elementen geplaatst worden.|
|element ~ element|`div ~ ul`|Selecteert alle `<ul>` elementen die voorafgegaan worden (op hetzelfde niveau) door een `<div>` element.|


Met de **universele selector**  `*` selecteren we alle elementen. We kunnen hiermee ook alle elementen selecteren binnen een bepaalde elementen. Wordt vaak toegepast om uniforme stijlen toe te passen op alle elementen.

```css
* {
box-sizing:border-box;
}
```
In responsive webdesign behoren de padding en de borders van de meeste elementen tot de opgegeven breedte van deze elementen, vandaar de bovenstaande instelling.



```html
<article id="article-main">The first paragraph.</article>

<h2>List 1</h2>
<ul>
  <li>JavaScript</li>
  <li>HTML</li>
  <li>CSS</li>
</ul>

<h2>List 2</h2>
<ul>
  <li>Twitter Bootstrap</li>
  <li>AngularJS</li>
  <li>Gulp</li>
</ul>
```

```CSS
article#article-main ~ ul {
    background: #f60;
}
```

> **Het resultaat in de webbrowser:**
>
>![enter image description here](https://lh5.googleusercontent.com/-FBRuobOuNrU/VBbWvMXSnOI/AAAAAAAAAM4/4hJcSybfM7c/s0/css_selectors_1.PNG "css_selectors_1.PNG")


Attribuut selectoren
------------------------

Attribuut selectoren selecteren een element door de aanwezigheid van een attribuut of attribuut waarde. De stelling binnen de vierkante haken ´[´ en ´]´ moet waar zijn. 

```
selector[attr] { 
property: value; 
}
```

- `[attr]`  
Representeert de elementen die het opgegeven attribuut bevatten.
- `[attr=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de waarde exact gelijk is aan de opgegeven waarde.
- `[attr~=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de waarde exact voorkomt in een lijst van woorden, waarbij woorden worden gescheiden door een spatie (whitespace-seperated).
- `[attr|=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de waarde exact  gelijk is aan de opgegeven waarde of exact gelijk is aan de waarde gevolgd door een koppelteken `-` (U+002D).
- `[attr^=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de waarde begint met de opgegeven waarde (prefix).
- `[attr$=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de waarde eindigt met de opgegeven waarde (suffix).
- `[attr*=value]`  
Representeert de elementen die het opgegeven attribuut bevatten en waarvan de opgegeven waarde voorkomt in de waarde van het opgegeven attribuut.

```
fdfsd
```

CSS pseudo elements and classes
----------------------------------------

### Pseudo-elementen

Pseudo-elementen beschrijven niet een speciale status van een selector, maar laten wel toe om **bepaalde delen van een document te stijlen**.

```
selector::pseudo-element { 
property: value; 
}
```

Twee dubbelpunten `::` wordt gebruikt om een pseudo-element aan te duiden in CSS, een enkel 
`:` wordt gebruikt om een pseudo-klasse te selecteren.

Mogelijke CSS pseudo-elementen:

- `::after`
- `::before`
- `::first-letter`
- `::first-line`
- `::selection`

####::before

Het pseudo-element `::before` maakt een schijn html-element aan dat als eerste kind fungeert van het geselecteerde html-element (via de selector). Wordt vaak gebruikt om extra inhoud via CSS toe te voegen aan een html-element. Het schijn-element is standaard een inline-element.

```
HTML:

<div class="pseudo1">
  <q>Some quotes</q>, he said, <q>are better than none</q>
</div>
```

```
CSS:

.pseudo1 q::before { 
  content: "«";
  color: #81CFE0;
}
.pseudo1 q::after { 
  content: "»";
  color: #EF4836;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh5.googleusercontent.com/-QQk_vVqUCQg/VBK_MSbx9AI/AAAAAAAAAI4/0jnHZQSlDlg/s0/css_pseudo_elements_1.PNG "css_pseudo_elements_1.PNG")
>

```
HTML:

<div class="todolist">
  <h2>Todo Lijst</h2>
    <ul>
      <li class="finished">Ontbijt eten</li>
      <li>Baard afscheren</li>
      <li>Linus naar de crêche brengen</li>
      <li>Cursus: NMDAD-I geven</li>
    </ul>        
</div>
```

```
CSS:

.todolist ul{
  list-style:none;
  margin:0;
  padding:0;  
}

.todolist ul>li{
  position:relative;
  background:#D2D7D3;
  margin-bottom:6px;
  padding:6px 12px 6px 32px;
}

.todolist ul>li.finished{
  background:#C8F7C5;
  margin-bottom:6px;
}

.todolist ul>li.finished::before{
  position:absolute;
  content:'✓';
  color:#16A085;
  font-weight:800; 
  top:6px;
  left:12px;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh4.googleusercontent.com/-iKxeYzHXsYo/VBLGGbiEn2I/AAAAAAAAAJM/rV1aW8X48wk/s0/css_pseudo_elements_2.PNG "css_pseudo_elements_2.PNG")
>

```javascript
Beetje interactiviteit toevoegen via JavaScript:

var todolist = document.querySelector('.todolist>ul');
todolist.addEventListener('click', function(ev) {
  if( ev.target.tagName === 'LI') {
     ev.target.classList.toggle('finished'); 
  }
}, false);
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh6.googleusercontent.com/-cwr_xym2G9g/VBLGerPpfHI/AAAAAAAAAJY/kxy4Qh-BXOs/s0/css_pseudo_elements_3.PNG "css_pseudo_elements_3.PNG")

####Intermezzo Unicode

- <http://unicode-table.com/en/#miscellaneous-symbols>
- <http://www.codecademy.com/>
- <http://text-symbols.com/cool/>

####::after

Het pseudo-element `::after` maakt een schijn html-element aan dat als laatste kind fungeert van het geselecteerde html-element (via de selector). Wordt vaak gebruikt om extra inhoud via CSS toe te voegen aan een html-element. Het schijn-element is standaard een inline-element.

```
HTML:

<div class="article">
  <p class="like">
    Ik ben fier om student te zijn in de opleiding Bachelor in de grafische en digitale media.
  </p>
  <p class="dislike">
    Ik zou niet fier zijn als ik student zou zijn in andere multimedia opleidingen, zoals ... .
  </p>
</div>
```

```
CSS:

.article .like::after{
  content:'▲';
  color:#16A085;
}

.article .dislike::after{
  content:'▼';
  color:#EF4836;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh5.googleusercontent.com/-PnsVPsHhlCI/VBLLNd0hODI/AAAAAAAAAJ0/sfbCRy4e4ZQ/s0/css_pseudo_elements_4.PNG "css_pseudo_elements_4.PNG")
>

```
HTML:

<div class="article">
  <p>De opleiding <span data-descr="GDM staat voor Grafische en Digitale Media. Het is een mega-unieke opleiding in dit universum, waar talenten worden getriggerd!">GDM</span> is de meest stijgende opleiding in het academiejaar <span data-descr="2014-15 zal een jaar worden vol met multimediaal plezier en vertier">2014-15</span>!
</div>
```

```
CSS:

.article span[data-descr]{
  position:relative;
  background:#C5EFF7;
  padding:6px;
}

.article span[data-descr]:hover::after{
  position:absolute;
  content:attr(data-descr);
  min-width:240px;
  background:#2C3E50;
  color:#fff;
  padding:6px 12px;
  border-radius:6px;
  left:0;
  top:24px;
}
```

Met de `attr()` CSS-expressie kunnen we de waarde van een bepaald attribuut opvragen. In dit geval vragen we de waarde op van het attribuut `data-descr`. Dat attribuut is een speciaal attribuut, namelijk een data-attribuut. Deze attributen maten toe om extra informatie uit te wisselen tussen de HTML en de DOM representatie.

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh3.googleusercontent.com/-T0KNaA0rj2w/VBLS7X9jBXI/AAAAAAAAAKI/xE7N_7YyCx0/s0/css_pseudo_elements_5.png "css_pseudo_elements_5.png")
>

####::first-letter

Met het pseudo-element `::first-letter` selecteren we de eerste letter van de eerste lijn van een blok-element. Blok-elementen zijn elementen die een blok innemen binnen een HTML-webpagina via de display-eigenschap met als mogelijke waarden: `block, inline-block, table-cell, list-item of table-caption`.

```
HTML:

<div class="articlefs">
  <p>In rhoncus egestas mauris, sit amet malesuada nibh congue in. Duis elit sem, ornare et semper a, sagittis vel tortor. Donec condimentum pretium orci, id lacinia massa feugiat vel. Ut ac magna erat, et blandit ligula. In rhoncus egestas mauris, sit amet malesuada nibh congue in. Duis elit sem, ornare et semper a, sagittis vel tortor. Donec condimentum pretium orci, id lacinia massa feugiat vel. Ut ac magna erat, et blandit ligula.</p>
<p>Etiam enim odio, vehicula non viverra in, placerat vel libero.</p>
</div>
```

```
CSS:

.articlefs::first-letter{
  background-color: #2C3E50;
  color: #FFF;
  float: left;
  font-size: 48px;
  margin-right: 10px;
  margin-top: 7px;
  padding: 18px;
  border-radius: 5px;
  box-shadow: 0 0 10px -2px #999999;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh4.googleusercontent.com/-zaCDmgGI8qQ/VBLZNG31GcI/AAAAAAAAAKc/LceUa9HBD5g/s0/css_pseudo_elements_6.png "css_pseudo_elements_6.png")
>

####::first-line

Met het pseudo-element `::first-line` selecteren we de eerste lijn van een blok-element. De hoeveelheid tekst op de eerste regel is afhankelijk van de breedte van het HTML-element en de tekstgrootte.

```
HTML:

<div class="articlefl">
  <p>Kun jij je geen leven zonder het web voorstellen? Kan je niet door een krant of tijdschrift bladeren zonder te focussen op lay-out en vormgeving? Of zie je jezelf als marketingstrateeg de crossmediaconcepten van de toekomst bedenken? Een unieke opleiding vol projecten en contacten met de bedrijfswereld staat klaar om van jou een gedreven professional te maken.</p>
</div>
```

```
CSS:

.articlefl{
  font-size:1.26em;
}

.articlefl::first-line{
  background:#D2D7D3;
  color:#2C3E50;
  font-weight:800;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh3.googleusercontent.com/-56N5HYKcRsM/VBLb_wdtqbI/AAAAAAAAAKs/qbeb2BKaPMw/s0/css_pseudo_elements_7.png "css_pseudo_elements_7.png")
>

####::selection

Met het pseudo-element `::selection` passen we stijlen toe op alle tekst die door de gebruiker geselecteerd (highlighted) is. Een beperkte verzameling van CSS-eigenschappen kunnen toegepast worden, zoals: `color, background, background-color en text-shadow`. Alle andere CSS-eigenschappen voor deze selector worden genegeerd.

```
HTML:

<div class="articless">
  <p>Je start met een brede basisopleiding die via afstudeerrichtingen en keuzetrajecten inspeelt op jouw talenten of specialisatiewens. Via uiteenlopende projecten, hetzij individueel of in team, bereiden we je voor op de uitdagingen van het werkveld. En wie weet groeit uit deze eerste contacten wel een toekomstig partnership of klantenrelatie? Aan interessante belevingen en werkervaring alvast geen gebrek tijdens deze opleiding!</p>
</div>
```

```
CSS:

.articless{
  font-size:1.28em;
}

.articless *::selection{
  background:#674172;
  color:#DCC6E0;
}

.articless *::-moz-selection{
  background:#674172;
  color:#DCC6E0;
}
```

> **Het resultaat in de webbrowsers:**
> 
> ![enter image description here](https://lh3.googleusercontent.com/--mXT03QBuwg/VBLhds3698I/AAAAAAAAALE/dDI2-I7Feeg/s0/css_pseudo_elements_8.png "css_pseudo_elements_8.png")
>

###Pseudo-klassen

Een CSS pseudo-class is een keyword toegevoegd aan selectoren waarmee we een speciale status van het element specificeren. De status `:hover` bijvoorbeeld wordt toegepast wanneer de gebruiker over het element beweegt.

```
Syntax:

selector:pseudo-class { 
property: value; 
}
```

####Link pseudo-klassen

Een link binnen HTML kan één van de volgende statussen bevatten:

- `:link`
Een standaard link die nog niet werd bezocht.
- `:visited`
Een link die reeds bezocht werd. Zit in de geschiedenis van de webbrowser.
- `:hover`
Wanneer de gebruiker over de link beweegt.
- `:active`
Wanneer de link geselecteerd wordt.

```css
a:link {color:#FF0000;}      /* unvisited link */
a:visited {color:#00FF00;}  /* visited link */
a:hover {color:#FF00FF;}  /* mouse over link */
a:active {color:#0000FF;}  /* selected link */
```
De volgorde van implementatie van deze statussen is van belang (LVHA-volgorde): `:visted` moet na `:link` gedefinieerd worden, `:hover` na `:link` en `:visted` en tenslotte de pseudo-klasse `:active` na `:hover`. Deze statussen wordt voornamelijk toegepast bij anchors en buttons.

####Form-elements pseudo-klassen

Er zijn een aantal pseudo-klassen die vooral van toepassing zijn op formulier-elementen, zoals: 

- `:focus`
Element ontvangt focus door selectie van de gebruiker via toetsenbord of muis.
- `:enabled`
Element is enabled indien de gebruiker dit element kan activeren, wijzigen of de focus kan ontvangen. 
- `:disabled`
Element is disabled indien de gebruiker dit element **niet** kan activeren, wijzigen of de focus kan ontvangen.
- `:required`
Invoerelement dat het **required-attribuut** bevat.
- `:valid`
Invoerelement waarvan de **inhoud correct gevalideerd** wordt volgens het gedefinieerde type.
- `:invalid`
Invoerelement waarvan de **inhoud niet correct gevalideerd** wordt volgens het gedefinieerde type. De gebruiker krijgt feedback om deze fouten te identificeren en vervolgens op te lossen.
- `:in-range`
Invoerelement waarvan de waarde **binnen de gespecificeerde grenzen** valt.
- `:out-of-range`
Invoerelement waarvan de waarde **buiten de gespecificeerde grenzen** valt.
- `:checked`
Radio-, Checkbox- of Option-element dat de on state bevat door selectie.


##Bibliografie

> **Pseudo-elementen:**
>  
> - http://www.w3schools.com/css/css_pseudo_elements.asp
- https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements
- http://www.fileformat.info/info/unicode/char/2713/index.htm
- http://www.fileformat.info/info/unicode/char/search.htm
- https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes#attr-data-*
- http://www.kirupa.com/html5/making_the_first_letter_stand_out.htm
- http://dev.w3.org/csswg/css-values/
- https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes
- http://www.w3schools.com/cssref/trysel.asp
>
> **Attribuut selectoren:**
>  
> - http://www.w3schools.com/css/css_attribute_selectors.asp
- http://www.w3schools.com/css/css_attribute_selectors.asp
- https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors
- http://www.css3.info/preview/attribute-selectors/