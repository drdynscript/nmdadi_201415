/*
Created by: Philippe De Pauw - Waterschoot
Date: 05-11-2014
Name: app.js
Description: Contact application
*/
(function(){
    var App = {
        init:function(){
            //Create the ContactDBContext object via a clone
            this.contactDBContext = ContactDBContext;
            this.contactDBContext.init('dds.contactapp');//Initialize the ContactDBContext
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Render the interface
            this.render();
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('contacts');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('contacts');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route
            var contactDetailsRoute = this.router.addRoute('/contact-details/{id}');//Add the route to crossroads
            contactDetailsRoute.matched.add(function(id){
                var contact = self.contactDBContext.getContactById(id);
                self.renderContactDetails(contact);

                //Set the active page where id is equal to the route
                self.setActivePage('contact-details');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('contact-details');

            });//Hash matches to section route
            //this.router.routed.add(console.log, console);//Log all crossroads events

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        cacheElements:function(){
            this.formContactCreate = document.querySelector('#formContactCreate');
            this.contactsList = document.querySelector('#contacts-list');
            this.contactDetails = document.querySelector('#contact-details-content');
            this.createContact = document.querySelector('#create-contact');
        },
        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Submit the contact create form --> listen to it!
            this.formContactCreate.addEventListener('submit', function(ev){
                //Dismiss the default browser eventlistener for submit event
                ev.preventDefault();

                //Check Validity
                console.log(this.checkValidity());

                //Create a new Contact Object
                var contact = new Contact();
                contact.id = Utils.guid();
                contact.firstname = this.elements["txtContactFirstName"].value;
                contact.surname = this.elements["txtContactSurname"].value;
                contact.mobile = this.elements["txtContactMobile"].value;
                contact.email = this.elements["txtContactEmail"].value;
                contact.created = new Date().getTime();

                //Add Contact to the database via Context
                var result = self.contactDBContext.addContact(contact);

                //Refresh the user interface -> render
                self.render();

                //Go to list
                self.hash.setHash('contacts');

                return false;
            });

            //Back buttons
            var backButtons = document.querySelectorAll('.back');
            if(backButtons != null && backButtons.length > 0){
                _.each(backButtons, function(backButton){
                    backButton.addEventListener('click', function(ev){
                        ev.preventDefault();

                        window.history.go(-1);//Back to the previous page (HTML5 History API

                        return false;
                    });
                });
            }

            //Create New Contact
            this.createContact.addEventListener('click', function(ev){
                ev.preventDefault();

                self.hash.setHash('contact-create');

                return false;
            });
        },
        render:function(){
            this.renderContacts();//Render the contacts
        },
        renderContacts:function(){
            this.contactsList.innerHTML = '';
            var html = '';

            var contacts = this.contactDBContext.getContacts();

            if(contacts != null && contacts.length > 0){
                _.each(contacts, function(contact){
                    html += ''
                        + '<article class="contact" data-id="' + contact.id + '" >'
                        + ((contact.avatar == null)?'<i class="avatar fa fa-user"></i>':'')
                        + '<span class="name">' +  contact.firstname + ' ' + contact.surname + '</span>'
                        + '</article>'
                });
            }else{
                html += '<div>No contacts available!</div>'
            }

            this.contactsList.innerHTML = html;

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Register listeners for all the contacts
            var nodes = document.querySelectorAll('.contact');
            if(nodes != null && nodes.length > 0){
                _.each(nodes,function(node){
                    node.addEventListener('click', function(ev){
                        //Get the id of the contact by the dataset property of the data attributes
                        var id = this.dataset.id;
                        self.hash.setHash('contact-details/' + id);
                    });
                });
            }
        },
        renderContactDetails:function(contact){

            this.contactDetails.innerHTML = '';
            this.contactDetails.dataset.id = contact.id;

            var html = ''
                + '<section class="contact-details-personal">'
                + ((contact.avatar == null)?'<i class="avatar fa fa-user"></i>':'')
                + '<div class="info">'
                + '<span class="name">' +  contact.firstname + ' ' + contact.surname + '</span>'
                + '</div>'
                + '</section>'
                + '<section class="contact-details-mobilephone">'
                + '<h3>Mobile</h3>'
                + '<p>' + ((contact.mobile != null && contact.mobile.length > 0)?contact.mobile:'Mobile phonenumber not specified!') + '</p>'
                + '</section>'
                + '<section class="contact-details-email">'
                + '<h3>Email</h3>'
                + '<p>' + ((contact.email != null && contact.email.length > 0)?contact.email:'Email not specified') + '</p>'
                + '</section>'

            this.contactDetails.innerHTML = html;
        },
        //Event Listener: listen to matched section routes (parse)
        onSectionMatch:function(section){
            //Set the active page where id is equal to the route
            this.setActivePage(section);
            //Set the active menuitem where href is equal to the route
            this.setActiveNavigationLink(section);
        },
        setActivePage:function(section){
            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages != null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id == section){
                        page.classList.add('active');
                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.page .main-navigation ul li a');
            if(navLinks != null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') == effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        }
    };

    App.init();//Initialize the application
})();