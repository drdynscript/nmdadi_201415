/*
Created by: Philippe De Pauw - Waterschoot
Date: 19-11-2014
Name: app.js
Description: Contact application
*/
(function(){
    var App = {
        init:function(){
            //Create the RandomUserMeAPI object via a clone
            this.randomUserMeAPI = RandomUserMeAPI;
            this.randomUserMeAPI.init('http://api.randomuser.me/?format=json');//Initialize the RandomUserMeAPI
            //Create UsersDBContext object via a clone
            this.usersDBContext = UsersDBContext;
            this.usersDBContext.init('dds.randomuserme');//Initialize the UsersDBContext
            //Create Handlebars Cache for templates
            this.hbsCache = {};
            //Cache the most important elements
            this.cacheElements();
            //Bind Events to the most important elements
            this.bindEvents();
            //Setup the routes
            this.setupRoutes();
            //Get Users
            if(this.usersDBContext.getUsers() == null){
                this.getUsersFromApi(100);
            }
            //Render the interface
            this.render();
        },
        cacheElements:function(){

        },
        bindEvents:function(){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            document.querySelector('#sync-randomuserme').addEventListener('click', function(ev){
                ev.preventDefault();

                self.getUsersFromApi(100);

                return false;
            });
        },
        setupRoutes:function(){
            this.router = crossroads;//Clone the crossroads object
            this.hash = hasher;//Clone the hasher object

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            //Crossroads settings
            var homeRoute = this.router.addRoute('/',function(){
                //Set the active page where id is equal to the route
                self.setActivePage('users');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('users');
            });
            var sectionRoute = this.router.addRoute('/{section}');//Add the section route to crossroads
            sectionRoute.matched.add(function(section){
                //Set the active page where id is equal to the route
                self.setActivePage(section);
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink(section);
            });//Hash matches to section route
            var userDetailsRoute = this.router.addRoute('/users/{username}');//Add the route to crossroads
            userDetailsRoute.matched.add(function(username){
                //Get the user by username
                var user = self.usersDBContext.getUserByUsername(username);
                //Render the details of a user
                self.renderUserDetails(user);

                //Set the active page where id is equal to the route
                self.setActivePage('user-details');
                //Set the active menuitem where href is equal to the route
                self.setActiveNavigationLink('users');

            });//Hash matches to section route
            //this.router.routed.add(console.log, console);//Log all crossroads events

            //Hash settings
            this.hash.initialized.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse initial hash
            this.hash.changed.add(function(newHash, oldHash){self.router.parse(newHash);});//Parse changes in the hash
            this.hash.init();//Start listening to the hashes
        },
        setActivePage:function(section){
            //Set the active page where id is equal to the route
            var pages = document.querySelectorAll('.page');
            if(pages != null && pages.length > 0){
                _.each(pages,function(page){
                    if(page.id == section){
                        page.classList.add('active');
                    }else{
                        page.classList.remove('active');
                    }
                });
            }
        },
        setActiveNavigationLink:function(section){
            //Set the active menuitem where href is equal to the route
            var navLinks = document.querySelectorAll('.page .main-navigation ul li a');
            if(navLinks != null && navLinks.length > 0){
                var effLink = '#/' + section;
                _.each(navLinks,function(navLink){
                    if(navLink.getAttribute('href') == effLink){
                        navLink.parentNode.classList.add('active');
                    }else{
                        navLink.parentNode.classList.remove('active');
                    }
                });
            }
        },
        render:function(){
            var p = this.usersDBContext.getUsersAsPaged(1, 20);
            console.log(p);

            //Render the users
            this.renderUsers(this.usersDBContext.getUsers());

            //Render favos
            this.renderFavos(this.usersDBContext.getFavoUsers());
        },
        renderUsers:function(users){
            if(!this.hbsCache['users']){//Cache the Users Template
                var src = document.querySelector('#users-template').innerHTML;
                this.hbsCache['users'] = Handlebars.compile(src);
            }
            document.querySelector('#users-list').innerHTML = this.hbsCache['users'](users);
        },
        renderUserDetails:function(user){
            if(!this.hbsCache['userdetails']){//Cache the Users Template
                var src = document.querySelector('#userdetails-template').innerHTML;
                this.hbsCache['userdetails'] = Handlebars.compile(src);
            }
            document.querySelector('#user-details-content').innerHTML = this.hbsCache['userdetails'](user);

            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            var star = document.querySelector('.add-user-to-favos');
            star.addEventListener('click', function(ev){
                ev.preventDefault();

                var isFavo = this.dataset.isfavo, username = this.dataset.username, result;

                if(isFavo == 'true'){
                    result = self.usersDBContext.removeUserFromFavosByUsername(username);
                }else{
                    result = self.usersDBContext.addUserToFavosByUsername(username);
                }

                if(result == 1){
                    var user = self.usersDBContext.getUserByUsername(username);
                    self.renderUserDetails(user);
                    self.renderFavos(self.usersDBContext.getFavoUsers());
                }

                return false;
            });
        },
        renderFavos:function(users){
            if(!this.hbsCache['users']){//Cache the Users Template
                var src = document.querySelector('#users-template').innerHTML;
                this.hbsCache['users'] = Handlebars.compile(src);
            }
            document.querySelector('#users-favos-list').innerHTML = this.hbsCache['users'](users);
        },
        getUsersFromApi:function(amount){
            //Get the reference to this App
            //Fix this in nested Event Listeners by use of self or that
            var self = this;

            this.randomUserMeAPI.getUsers(amount).then(
                function(data){
                    //Get the value of the key results
                    var results = data.results;
                    //Loop through the objects within the results --> Array
                    _.each(results, function(obj){
                        self.usersDBContext.addUser(obj.user);
                    });
                    //Get all users
                    var users = {
                        "users":self.usersDBContext.getUsers()
                    }
                    //Render the users
                    self.renderUsers(users);
                },
                function(result){
                    console.log(result);
                }
            );
        }
    };

    App.init();//Initialize the application
})();