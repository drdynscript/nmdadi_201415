Handlebars.registerHelper('fullName', function(person) {
    return person.title.capitalize() + ". " + person.first.capitalize() + " " + person.last.capitalize();
});

Handlebars.registerHelper('dayOfBirth', function(dob) {
    var date = new Date(dob*1000);
    return date.toLocaleDateString();
});

Handlebars.registerHelper('address', function(location) {
    return location.street + ' ' + location.zip + ' ' + location.city.capitalize()
});
