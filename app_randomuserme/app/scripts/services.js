/**
 * Created by Administrator on 5/11/14.
 */
var RandomUserMeAPI = {
    init:function(connString){
        this.connString = connString;
    },
    getUsers:function(amount){
        var url = this.connString + '&results=' + amount;
        return Utils.getJSONByPromise(url);
    }
};

var UsersDBContext = {
    init:function(connString){
        this.connString = connString;
        //Create the AppData object for the Users Application
        this.AppData = {
            "information":{
                "title":"Users Application",
                "version":"1.0",
                "modified":"16-11-2014",
                "author":"Philippe De Pauw - Waterschoot"
            },
            "users":[],
            "favos":[],
            "settings":{}
        };
        //Get Application Data from the localstorage
        if(Utils.store(this.connString) != null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
    getUsers:function(){
        var users = this.AppData.users;

        if(users == null || (users != null && users.length == 0))
            return null;

        users = _.sortBy(users,'registered');
        return {"users":users};
    },
    getUsersAsPaged:function(page, pageSize){
        var users = this.AppData.users;

        if(users == null || (users != null && users.length == 0))
            return null;

        users = _.sortBy(users,'registered');

        var maxPages = Math.floor((users.length-1)/pageSize);
        var realPage = ((page <= maxPages)?page:maxPages);
        var filteredUsers = users.slice((realPage-1)*pageSize, (realPage-1)*pageSize+pageSize);

        var pagedUsers = {
            "page":realPage,
            "pageSize":pageSize,
            "totalItems":users.length,
            "maxPages":maxPages,
            "users":filteredUsers
        }

        return pagedUsers;
    },
    getFavoUsers:function(){
        var favos = this.AppData.favos;
        if(favos == null || (favos != null && favos.length == 0))
            return false;

        //Get the reference to this App
        //Fix this in nested Event Listeners by use of self or that
        var self = this;

        var users = [], user;
        _.each(favos, function(username){
            user = self.getUserByUsername(username);

            if(user != null)
                users.push(user);
        });
        users = _.sortBy(users,'registered');
        return {"users":users};
    },
    isUserFavoByUsername:function(username){
        var favos = this.AppData.favos;
        if(favos == null || (favos != null && favos.length == 0))
            return false;

        var user = _.find(favos, function(uname){
           return uname == username;
        });

        if(typeof user == 'undefined')
            return false;

        return true;
    },
    getUserByUsername:function(username){
        var user = _.find(this.AppData.users, function(user){
           return user.username == username;
        });//Find a user by his/here username

        if(typeof user == 'undefined')
            return null;//Make null <-- undefined object

        user.isFavo = this.isUserFavoByUsername(username);

        return user;
    },
    addUser:function(user){
        if(this.getUserByUsername(user.username) != null)
            return 0;

        this.AppData.users.push(user);
        this.save();

        return 1;
    },
    addUserToFavosByUsername:function(username){
        if(this.isUserFavoByUsername(username))
            return 0;

        this.AppData.favos.push(username);
        this.save();

        return 1;
    },
    removeUserFromFavosByUsername:function(username){
        if(!this.isUserFavoByUsername(username))
            return 0;

        var index = _.findIndex(this.AppData.favos, function(uname){
            return uname == username;
        });

        if(index == -1)
            return 0;

        this.AppData.favos.splice(index, 1);
        this.save();

        return 1;
    },
    save:function(){
        this.AppData.modified = new Date().getTime();
        Utils.store(this.connString, this.AppData);//Save all data to the localstorage
    }
};