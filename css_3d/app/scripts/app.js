/*
Created by: Philippe De Pauw - Waterschoot
Date: 30/09/2014
Name: app.js
Description: JavaScript for the application
*/
(function(){
    var draggables = document.querySelectorAll('[draggable="true"]');
    if(draggables != null){
        for(var i=0;i<draggables.length; i++){
            draggables[i].addEventListener('dragstart',dragStart,true);
            draggables[i].addEventListener('drag',drag,true);
            draggables[i].addEventListener('dragend',dragEnd,true);
        }
    }

    var dropzone = document.querySelector('.dropzone');
    dropzone.addEventListener('dragenter',dragEnter,false);
    dropzone.addEventListener('dragover',dragOver,false);
    dropzone.addEventListener('dragleave',dragLeave,false);
    dropzone.addEventListener('drop',dragDrop,false);

    function dragStart(ev){
        console.log('DRAG START');

        ev.dataTransfer.dropeffectAllowed = 'copyMove';

        dropzone.classList.remove('dragenter');
        dropzone.classList.remove('dragdrop');
    }

    function drag(ev){
        //console.log(ev);//Nooit logging in drag event!
    }

    function dragEnd(ev){
        console.log('DRAG END');
    }

    function dragEnter(ev){
        ev.preventDefault();

        console.log('DRAG ENTER');

        ev.dataTransfer.dropEffect = 'copy';

        this.classList.toggle('dragenter');

        return false;
    }

    function dragOver(ev){
        ev.preventDefault();

        //console.log('DRAG OVER');//Nooit logging in drag over!

        ev.dataTransfer.dropEffect = 'copy';

        return false;
    }

    function dragLeave(ev){
        ev.preventDefault();

        console.log('DRAG LEAVE');
        this.classList.toggle('dragenter');

        return false;
    }

    function dragDrop(ev){
        ev.preventDefault();

        console.log('DRAG DROP');
        this.classList.toggle('dragenter');
        this.classList.toggle('dragdrop');
        dropzone.addEventListener('transitionend mozTransitionEnd msTransitionEnd oTransitionEnd webkitTransitionEnd',function(ev){
            console.log('TRANSITION END');
        }, false)

        return false;
    }
})();