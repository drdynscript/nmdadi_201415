﻿NMDAD-I 2014-15
===============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|
|Hogeschool|Arteveldehogeschool|

***

[TOC]

***

|Info||
|--|--|
|Studiepunten|6|
|Contacturen|4|
|Docent|Philippe De Pauw - Waterschoot|
|Evaluatie 1ste examenkans|Werkstuk(ken) (maximaal in groepjes van 3). Mondelinge verdediging van werkstuk(ken) + vragen|
|Evaluatie 2de examenkans|Werkstuk(ken) (Individueel). Mondelinge verdediging van werkstuk(ken) + vragen|
|Begincompetenties|2D Animation & Webdesign II|
|ECTS|[ECTS Fiche NMDADI](http://www.arteveldehogeschool.be/ects/ahsownapp/ects/ECTSFiche.aspx?olodID=42086)|


##Opdracht 2e examenkans: Open the gates for data!

|Info||
|----|--|
|Werktitel|Open the gates for data!|
|Subtitel|Visualisatie van open data in een App|
|Data eis|Gebruik minimaal 10 datasets uit de websites: http://data.gent.be/datasets, http://datatank4.gent.be/, http://appsforghent.be/|
|Briefing|**06-05-2015** Briefing opdracht 2e examenkans|
|Deadline|**16-08-2015** Opleveren opdracht 1|
|Examen|Modelinge verdediging van werkstuk. Ongeveer 15min per student.

###Productiedossier

- Moet geschreven worden in Markdown
- Briefing
- Analyse
- Technische specificaties
- Functionele specificaties
- Persona's (+ scenario)
- Ideëenborden
- Sitemap
- Wireframes
- Style tiles
- Visuele designs
- Screenshots eindresultaat
- Screenshots code snippets

###Digitale structuur

- README.md (Markdown-bestand met informatie over de app en student)
- documents (folder)
	- dossier.pdf
	- dossier.md
	- poster.pdf
	- poster.png
- screenshots (folder)
- screencasts (folder)
- openthegates (folder - zie structuur frontend webapplicatie - zie structuur bitbucket)
	- app (folder)
	- bower_components (folder)
	- dist (folder)
	- node_modules (folder)
	- test (folder)
	- .editorconfig
	- .bowerrc
	- .gitattributes
	- .gitignore
	- .jscsrc
	- .jshintrc
	- bower.json
	- gulpfile.js
	- LICENSE
	- package.json
	- README.md

**Structuur van de app folder**

- app (folder)           
	- documents
	- fonts (webfonts)
	- images
	- icons
		   - favicon.ico
		   - verschillende touch iconen
	- scripts
		- app.js
		- googlemaps.js
		- helpers.js
		- models.js
		- services.js
		- utilities.js
		- ...
	- styles
		- app.css
		- buttons.css
		- colors.css
		- common.css
		- helpers.css
		- layout.css
		- lists.css
		- normalize.css
		- ...
	- 404.html
	- humans.txt
	- index.html
	- manifest.webapp
	- robots.txt
	- sitemap.xml

###Technische specificaties

- Versiebeheer
	- Maak een nieuw Git Repository voor je project aan op **Bitbucket**.
Code en andere assets worden tussen studenten niet gedeeld!
Deel het project met de docent(en).
- Frontend (One Page Applicatie)
	- HTML5, CSS3 en JavaScript
	- jQuery, underscore.js, lodash.js, crossroads.js, js-signals
	- localstorage of IndexedDB
- Automation verplicht!
    - Componenten worden via Bower toegevoegd in de components folder van de app folder
    - SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
    - CSS-bestanden worden met elkaar verbonden in één bestand en geminified
    - De JS code wordt automatisch nagekeken op syntax fouten
    - JS-bestanden worden met elkaar verbonden in één bestand en geminified
    - De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
    - Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken

> Webapplicatie moet de look-and-feel van een native applicatie bevatten! Het responsive- framework alsook alle andere bestanden moeten zelf geïmplementeerd worden. Tijdsbesteding moet in het dossier aanwezig zijn!

- <http://www.pttrns.com/>
- <http://www.mobile-patterns.com/>

###Functionele specificaties

- Gebruik minimaal 10 datasets uit de websites: http://data.gent.be/datasets, http://datatank4.gent.be/, http://appsforghent.be/
- Zorg visueel en inhoudelijk voor een coherent geheel tussen deze datasets. Het is dus ook een creatieve opdracht
- Data visualisaties
- Geolocatie
- Google Maps of Leaflet

###Persona's

- Gebruiker (User)

###Opleveren

- Digitaal
	- Bitbucket
		- Naam repository: nmdadi_201415_openthegates_{naam}  
		bv.: nmdadi_201415_openthegates_philippedepauw
	- Online via de Arteveldehogeschool hosting
		- ftp://ftp.arteveldehogeschool.be
		- folder: /campusGDM/studenten_201415/{loginnaam}/nmdadii/openthegates/  
		bv.: /campusGDM/studenten_201415/phildp/nmdadii/openthegates/  
		- Te bereiken via http://www.arteveldehogeschool.be/campusGDM/studenten_201415/phildp/nmdadii/openthegates/
- Afgedrukt
	- Mee te brengen op de examendag
	- Structuur dossier zie hoofdstuk Productiedossier
