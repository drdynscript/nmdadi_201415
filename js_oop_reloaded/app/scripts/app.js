/*
Created by: Philippe De Pauw - Waterschoot
Date: 04-11-2014
Name: app.js
Description: JavaScript OOP Reloaded
*/
(function(){
    /*
    Object Initializers
    = Literal notation
    = JSON(P)
     */
    var person = {
        "firstName":"Philippe",
        "surName":"Parent",
        "email":"jonas@howest.be",
        "isFemale":false,
        "toString":function(){
            return this.firstName + ' ' + this.surName;
        }
    };
    console.log(person.toString());
    console.log(person.firstName);

    var person1 = person;//Clone the Person
    person1.firstName = 'Annick';
    person1.surName = 'Pottie';
    person1.email = 'luc@hogent.be';
    person1.isFemale = true;
    console.log(person1);

    /*
    Object constructors
     */
    function Movie(){
        this.title;
        this.synopsis;
        this.year;
        this.toString = function(){
            return this.title;
        };
    }

    var movie1 = new Movie();//Make an instance of the (class) Movie via new keyword
    console.log(movie1);
    movie1.title = "De reis van Chihiro";
    movie1.synopsis = "In the middle of her family's move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and monsters; where humans are changed into animals; and a bathhouse for these creatures.";
    movie1.year = 2001;
    console.log(movie1);

    /*
    Encapsulation + Closures
     */
    function PC(){
        //Private Variables
        var _nCores = 32;
        var _memorySize = 128;
        var _ssd = 2000;
        //Private Methods
        function toString(){
            return 'PC: ' + _nCores + ' cores, ' + _memorySize + ' GB RAM';
        }
        //Public Methods aka Closures
        return {
            toString:toString,
            setNumberOfCores:function(value){
                _nCores = value;
            },
            getNumberOfCores:function(){
                return _nCores;
            }
        };
    }

    var raspBerryAmericanPie = new PC();
    console.log(raspBerryAmericanPie._nCores);
    console.log(raspBerryAmericanPie.toString());
    raspBerryAmericanPie.setNumberOfCores(69874);
    console.log(raspBerryAmericanPie.getNumberOfCores());

    /*
    Prototype
     */
    var Person = (function () {

        var _firstName, _surName;

        function Person(){}

        Person.prototype = {
            //Properties
            get FirstName(){return _firstName;},
            set FirstName(value){_firstName = value;},
            get SurName(){return _surName;},
            set SurName(value){_surName = value;}
        };

        Person.prototype.toString = function(){
            return 'Person: ' + _firstName + ' ' + _surName;
        }

        return Person;
    })();

    var Student = (function(_super){


        var _studentNr;

        Student.prototype = new Person();
        Student.prototype.constructor = Student;
        function Student() {
            for(var p in new Person()){
                console.log(p);
            }
            _super.call(this);
        }

        Student.prototype = {
            //Properties
            get StudentNr(){return _studentNr;},
            set StudentNr(value){_studentNr = value;}
        };

        Student.prototype.toString = function () {
            return _super.prototype.toString.call(this);
        };

        return Student;
    })(Person);

    var student = new Student();
    student.FirstName = 'Philippe';
    student.SurName = 'De Pauw';
    student.StudentNr = 896532;
    console.log(student);
})();