/*
Created by: Philippe De Pauw - Waterschoot
Date: 18-11-2014
Name: app.js
Description: Templates in JavaScript via Handlebars
*/

/*
Handlebars Helpers
 */
Handlebars.registerHelper('fullName', function(person){
    return person.firstName + ' ' + person.lastName;
});

Handlebars.registerHelper('faGender', function(isFemale){
    return '<i class="fa fa-user ' + ((isFemale)?'female':'male') + '"></i>';
});

(function(){
    //Example 1
    //===========================================================
    //1. Create the data (Later inserted into the template)
    var petObj = {
        "name":"Frodo",
        "weight":7.2,
        "color":"ginger"
    }
    //2. Get the contents of the source (script) of the handlebars template
    var src = document.querySelector('#pet-template').innerHTML;
    //3. Compile the source as a real Handlebars template
    var tmpl = Handlebars.compile(src);
    //4. Insert the data into the template --> HTML
    var html = tmpl(petObj);
    //5. Write the generated Html into a DOM-container
    document.querySelector('#hbs1').innerHTML = html;

    //Example 2
    //===========================================================
    var blogObj ={
        "posts":[
            {
                "id":1,
                "title":"Software-update: WordPress 4.1 bèta 1",
                "synopsis":"De eerste bètarelease van WordPress versie 4.1 is uitgekomen. Met dit programma, dat onder een gpl-licentie beschikbaar wordt gesteld, is het mogelijk om een weblog op te zetten en bij te houden.",
                "story":"<p>WordPress is eenvoudig in te stellen en kan binnen vijf minuten draaien als er al een server met php en MySQL beschikbaar is. Er zijn mogelijkheden om de functionaliteit van WordPress, naast bloggen, verder uit te breiden en het uiterlijk met plug-ins en themes aan te passen. Versie 4.1 bevat onder meer een nieuwe standaard thema. Verder is er een optie die alle toolbars laat verdwijnen als je tekst invoert, iets wat je beter zou moeten laten concentreren. De complete release notes kunnen hieronder worden gevonden.</p>",
                "created":new Date().getTime()
            },
            {
                "id":2,
                "title":"WhatsApp-ceo doneert 800.000 euro aan de FreeBSD Foundation",
                "synopsis":"De FreeBSD Foundation heeft een donatie van 1.000.000 dollar ontvangen van Jan Koum, de ceo en medeoprichter van WhatsApp. Het gaat omgerekend om een bedrag van 800.000 euro. Hij is daarmee verantwoordelijk voor de grootste donatie sinds de oprichting van de organisatie.",
                "story":"<p>Het is onduidelijk wat er exact met het geld gaat gebeuren; de organisatie heeft een team opgericht om zich hierover te buigen. Het streven is dat de effecten van de donatie nog jarenlang zichtbaar blijven. Zo moet de snelheid van de groei van de organisatie worden bevorderd, waardoor nieuwe mogelijkheden en andere services ontstaan. Het is onbekend hoe dat er in de praktijk uit gaat zien.</p>",
                "created":new Date().getTime()
            },
            {
                "id":3,
                "title":"Google brengt autoscaling naar Compute Engine",
                "synopsis":"Google heeft op zijn Compute Engine-platform de zogeheten Autoscaling-functionaliteit geactiveerd. Dit mechanisme kan op basis van diverse parameters, zoals het aantal requests, virtuele machines snel in- en uitschakelen. Volgens Google kan dit kostenbesparingen opleveren..",
                "story":"<p>De Compute Engine Autoscaler detecteert de vraag naar bijvoorbeeld een website aan de hand van diverse parameters. Zo kan het kijken naar de cpu-belasting van een groep beschikbare instances, virtuele machines binnen het Compute Engine-platform. Tevens kan de Autoscaler het aantal queries per seconde opvragen van http-loadbalancers. Deze informatie laat de Autoscaler bij grote drukte extra instances activeren, terwijl in daluren juist het aantal actieve instances dynamisch kan worden verminderd.</p>",
                "created":new Date().getTime()
            }
        ]
    };
    var src2 = document.querySelector('#blog-template').innerHTML;
    var tmpl2 = Handlebars.compile(src2);
    var html2 = tmpl2(blogObj);
    document.querySelector('#hbs2').innerHTML = html2;

    //Example 3
    //===========================================================
    var contactsObj = {
        "lists":[
            {
               "name":"personal",
               "contacts":[
                   {
                       "id":6,
                       "firstName":"Olivia",
                       "lastName":"Parent",
                       "isFemale":true
                   },
                   {
                       "id":10,
                       "firstName":"Philippe",
                       "lastName":"The Peacock - Watergate",
                       "isFemale":false
                   }
               ]
            },
            {
                "name":"work",
                "contacts":[
                    {
                        "id":2,
                        "firstName":"Philippe",
                        "lastName":"Parent",
                        "isFemale":false
                    },
                    {
                        "id":12,
                        "firstName":"Olivia",
                        "lastName":"De Pauw - Waterschoot",
                        "isFemale":true
                    }
                ]
            },
            {
                "name":"whishlist"
            }
        ]
    };
    var src3 = document.querySelector('#contacts-template').innerHTML;
    var tmpl3 = Handlebars.compile(src3);
    var html3 = tmpl3(contactsObj);
    document.querySelector('#hbs3').innerHTML = html3;
})()
