/*
 Created by: Philippe De Pauw - Waterschoot
 Date: 21-10-2014
 Name: app.js
 Description: Weather application
 WOEID Gent: 12591774
 WOEID Brussel: 968019
 WOEID Oostende: 975129
 WOEID Montreal: 3534
 Links:
 - https://developer.yahoo.com/yql/console/
 - https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm/related
 - https://weather.yahoo.com/forecast/BEXX0008_c.html
 - http://woeid.rosselliot.co.nz/lookup/
 */

(function(){
    var weatherURL = 'https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D{0}%20and%20u%3D%22c%22&format=json&diagnostics=true&callback=';
    //Global variable for the weather data
    var weatherData = null;

    //Function: Load weather based on the generated url
    function loadWeatherViaUrl(url){
        //1. Create a XMLHttpRequest object (Send to and load data from a WebAPI
        var xhr = typeof XMLHttpRequest != undefined
            ? new XMLHttpRequest()
            : new ActiveXObject('Microsoft.XMLHTTP');

        //2. Open the connection or tunnel to the resource on the url
        xhr.open('GET', url, true);
        //3. Declare the type of the response
        xhr.responseType = 'json';
        //4. Listen to the changes in states within the connection
        xhr.onreadystatechange = function(){
            switch(xhr.readyState){
                case 0:console.log('UNSET');break;
                case 1:console.log('OPENED');break;
                case 2:console.log('HEADERS RECEIVED');console.log(this.getAllResponseHeaders());break;
                case 3:console.log('LOADING');break;
                case 4:default:
                    console.log('LOADED');
                    //If status equals 200 then everything is ok else nok
                    if(xhr.status == 200){
                        console.log('OK');
                        //Get the received data --> response
                        var data = (!xhr.responseType)?JSON.parse(xhr.response):xhr.response;
                        //Assign the value of the local variable data to the global variable weatherData
                        weatherData = data;
                        //Call the function: consumeData
                        consumeData();
                    }else {
                        console.log(Error(xhr.status));
                    }
                    break;
            }
        };
        //5. Make the request to the specified resource
        xhr.send();
    }

    //Function: Load the weather from a certain city or region
    function loadWeatherFromWOEID(woeid){
        //Replace {0} by the woeid of the city
        var url = weatherURL.format(woeid);
        //Call the function: loadWeatherViaUrl, don't forget an argument
        loadWeatherViaUrl(url);
    }

    function consumeData(){
        console.log(weatherData);
        var weatherInformation = weatherData.query.results.channel.item.description;
        document.querySelector('#weather-information').innerHTML = weatherInformation;
    }

    //Call the function: loadWeatherFromWOEID, don't forget an argument
    loadWeatherFromWOEID(12591774);

})();