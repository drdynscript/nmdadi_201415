Front-end automation example project
====================================

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***

[TOC]

***

Projectstructuur
--------------------------------------

###Aanmaak van de noodzakelijke folders

We maken op een bepaalde locatie binnen een bepaald medium, zoals harde schijf, usb-stick, ..., de folder `project_cocoa` aan. Dat kan via een verkenner, IDE of via commandline.

- `mkdir project_cocoa`

We kunnen in deze folder geraken via commandline:

- `cd project_cocoa`

Binnen deze folder creëren we de sub-folders `project_cocoa_code` en `project_cocoa_docs`.

- `mkdir project_cocoa_code`
- `mkdir project_cocoa_docs`

De code-folder bevat de front-end applicatie, de docs-folder bestaat uit documenten, screenshots en een screencast.

We navigeren vervolgens naar de folder `project_cocoa_code`:

- `cd project_cocoa_code`

In deze folder maken we de sub-folders `app`, `dist` en `test` aan.

- `mkdir app`  
De `app`-folder bevat de applicatie in ontwikkeling.
- `mkdir dist`
De `dist`-folder bevat de applicatie, na het bouwen met `Gulp`, die we online op een server kunnen plaatsen. Kortom de uiteindelijke applicatie die een gebruiker kan bezoeken.
- `mkdir test`  
De `test`-folder bevat een verzameling van testen die we toegepast hebben op de **development** versie van de applicatie, de applicatie dus onder de `app`-folder.

We navigeren vervolgens naar de folder `app`:

- `cd app`

In deze folder maken we de noodzakelijke folders en bestanden aan, in ontwikkeling, die de vooropgestelde applicatie werkende maken. Zoals eerder toegepast in voorgaande projecten bestaat een front-end applicatie uit de folders:

* `documents`
Documenten de als link kunnen gerefereerd worden in html-bestanden
* `fonts`
De webfonts die we zullen gebruiken in de applicatie
* `images`
De afbeeldingen en iconen die als link kunnen gerefereerd worden in html-bestanden
* `scripts`
De JavaScript-bestanden die we zelf hebben geschreven.
* `styles`
De stijlbestanden die de applicatie zullen pimpen.

De commandline commando's om deze folders aan te maken:

- `mkdir documents`
- `mkdir fonts`
- `mkdir images`
- `mkdir scripts`
- `mkdir styles`

Om deze voorlopig lege folders toe te voegen aan GitHub of Bitbucket, maken we een README.md bestand of een `.gitkeep` bestand aan binnen iedere folder. In het `README.md` bestand vermenden we de functionaliteit van deze folder, ook voor de folder `project_cocoa_docs`.

### README.md bestand per folder

#### `app`-folder

> **README.md bestand**
>
```
Zal de volgende folders bevatten:

* documents
* fonts
* images
* scripts
* styles

Zal de volgende bestanden bevatten rechtstreeks onder deze folder:

* 404.html
* humans.txt
* index.html
* manifest.webapp
* robots.txt
* sitemap.xml

Bestanden eventueel aangevuld met favicon en touch icons.
```

#### `dist`-folder

> **README.md bestand**
>
```
**Toekomstige Unit Tests**
```

#### `project_cocoa_docs`-folder

> **README.md bestand**
>
```
Zal bevatten:

* dossier.md
* dossier.pdf
* poster.pdf
* poster.png
* screencast.mpeg
* screenshot_320.png
* screenshot_480.png
* screenshot_640.png
* screenshot_800.png
* screenshot_960.png
* screenshot_1024.png
* screenshot_1280.png
* screenshot_1440.png
```

#### `project_cocoa`-folder

> **README.md bestand**
>
```
De applicatie bestaat uit twee folders, namelijk:

* `project_cocoa_code`
Hierin voegen we de front-end applicatie toe!
* `project_cocoa_docs`
Bevat het dossier, screencast en screenshots.

Vervolgens omschrijven we de applicatie hierin, kort maar bondig.
```

In de andere folders kan je eventueel ook een README.md bestand toevoegen indien je deze lege folder toch wil aanmaken op GitHub en/of Bitbucket.

Bestanden onder de root van de webapplicatie
-------------------------------------------------------

###robots.txt

```
User-agent: *
Disallow: 404.html
Disallow: /components/
Disallow: /data/
Disallow: /docs/
Disallow: /fonts/
Disallow: /scripts/
Disallow: /styles/
Sitemap: /sitemap.xml
```

###humans.txt

```
```

Bower
-----

Configuratie van Bower per applicatie realiseren we op het niveau van de folder `project_cocoa_code` via het bestand `.bowerrc`. Dit bestand moeten we handmatig aanmaken. In dit bestand vermelden we de instellingen voor Bower.

```
{
    "directory":"bower_components",
    "analytics":false,
    "color":true,
    "proxy":"http://proxy.arteveldehs.be:8080",
    "https-proxy":"http://proxy.arteveldehs.be:8080"
}
```

In de eigenschap `directory` definiëren we de locatie, lees de folder, waarbinnen we de JavaScript bibliotheken zullen toevoegen, die de applicatie zal gebruiken. Voor deze applicatie en ook voor de opdracht 1: Cocoa, zullen we zeker de volgende bibliotheken toevoegen: modernizr, lodash, jQuery, Handlebars, crossroads, hasher en js-signals.

Andere instellingen vinden we via <http://bower.io/docs/config/>. In dit voorbeeld sturen van geen informatie door naar de Bower-cloud, zal de commandline de Bower-kleuren bevatten en voegen we de proxy-server toe van de Arteveldehogeschool (enkel indien we niet gebruik maken van wifi-verbinding Arteveldehs Open).

Op dezelfde plaats van `.bowerrc` maken we een `bower.json` bestand aan en dit via het commando `bower init`.

> **Bower initalisatie:**
> 
> ![bower init](https://lh5.googleusercontent.com/-nFnpB340Gu0/VIAvk83lGVI/AAAAAAAAAqE/ncgx28dKRuk/s0/bower_init.PNG "bower_init.PNG")

Onderstaand het aangemaakt `bower.json` bestand. Dit bestand bevat voorlopig geen **dependencies**.

```
{
  "name": "Cocoa",
  "version": "0.1",
  "description": "ToDo Application for the mass",
  "main": "app.js",
  "keywords": [
    "html5",
    "css3",
    "JavaScript"
  ],
  "authors": [
    "drdynscript",
    "Philippe De Pauw - Waterschoot"
  ],
  "license": "MIT",
  "private": true,
  "ignore": [
    "**/.*",
    "node_modules",
    "bower_components",
    "test",
    "tests"
  ],
  "homepage": "http://www.arteveldehogeschool.be"
}
```

###Aanmaak van de dependencies

`bower install --save-dev modernizr`  
`bower install --save-dev lodash`  
`bower install --save-dev handlebars`  
`bower install --save-dev crossroads`  
`bower install --save-dev hasher`  
`bower install --save-dev jquery`  

>
> 
> ![installatie dependencies via Bower](https://lh5.googleusercontent.com/-2rKK56iOh_Y/VIAyKGLoCoI/AAAAAAAAAqU/glNXIITK_G4/s0/bower_install.PNG "bower_install.PNG")

Het bestand `bower.json` werd door de bovenstaande handelingen aangepast. Het bevat **development dependencies** ondergebracht in de sleutel `devDependencies` met een `object` als waarde. Dit `object` bestaat een verzameling van key/value-pairs.
```
{
  ...
  "devDependencies": {
    "modernizr": "~2.8.3",
    "lodash": "~2.4.1",
    "handlebars": "~2.0.0",
    "crossroads": "~0.12.0",
    "hasher": "~1.2.0",
    "jquery": "~2.1.1"
  }
}
```


Gulp
------------

Via het commando `npm init` kunnen we een `package.json` bestand aanmaken.

> **npm init:**
>  
>  ![npm init](https://lh6.googleusercontent.com/-7LtEPpiX_SA/VIA54uTvhWI/AAAAAAAAAqs/4LEhrLn3bys/s0/npm_init.PNG "npm_init.PNG")

```
{
  "name": "project_cocoa_code",
  "version": "0.1.0",
  "description": "ToDo Application for the mass",
  "main": "app.js",
  "directories": {
    "test": "test"
  },
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "http://bitbucket.org/drdynscript/project_cocoa.git"
  },
  "keywords": [
    "html5",
    "css3",
    "JavaScript"
  ],
  "author": "Philippe De Pauw - Waterschoot aka drdynscript",
  "license": "MIT"
}
```

###Setup van Gulp

- `npm install --save-dev gulp`  
- `npm install --save-dev minimist`  
- `npm install --save-dev through2`  
- `npm install --save-dev gulp-load-plugins`  
- `npm install --save-dev gulp-size`

###Verwijderen van de inhoud van de folders `.tmp` en 'dist'.

Voordat we het de productieversie van de applicatie genereren, moet de inhoud van de folders `.tmp` en `dist` verwijderd worden. Dit kunnen we realiseren via het pakket `del` <https://www.npmjs.org/package/del>.

- `npm install --save-dev del` 

```

```

###JavaScript syntax nakijken

- `npm install --save-dev gulp-jshint`
- `npm install --save-dev jshint-stylish`

> **Resultaten na `gulp jshint`
>  
>  ![Resutaten na gulp jshint](https://lh3.googleusercontent.com/-0G60c8XwNsk/VIGKCVA4DxI/AAAAAAAAAro/O9JSyg1sCTk/s0/gulp_jshint.PNG "gulp_jshint.PNG")

###JavaScript 

- `npm install --save-dev gulp-useref`
- `npm install --save-dev gulp-if`
- `npm install --save-dev gulp-uglify`

.gitignore
------------




```
./idea
project_cocoa_code/.idea
project_cocoa_code/.sass-cache
project_cocoa_code/.tmp
project_cocoa_code/*.log
project_cocoa_code/bower_components
project_cocoa_code/dist
project_cocoa_code/node_modules
project_cocoa_docs/.idea
```

Bibliografie
-------------

> **Git:**
>  
- https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf
>  
> **npm:**
>  
- <http://browsenpm.org/help>
- <https://www.npmjs.org/>
- <https://www.npmjs.org/browse/star>
>   
> **PhantomJS:**
>   
- <http://phantomjs.org/>

> **Bower:**
>  
- <https://github.com/bower/bower>
- <http://sindresorhus.com/bower-components/>
- <http://bower.io/search/>
>    
> **Proxy:**
>  
- <http://hogart.blogspot.be/2014/09/git-npm-bower-and-corporate-proxies.html>



