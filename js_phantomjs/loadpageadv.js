var page = require('webpage').create(),
    system = require('system');

var start = Date.now();

if(system.args.length === 1){
    console.log('Usage: loadpageadv.js <some URL>');
    phantom.exit();
}

var url = system.args[1];

page.open(url, function(p){
    if(p !== 'success'){
        console.log('Failed to load the webpage!')
    }else{
        var end = Date.now();
        var diff = end - start;

        console.log('Time to load the page: ' + diff);
    }
    phantom.exit();
});